# Puzzler

Public url: https://ondrejskalicka.gitlab.io/puzzler/

## How to run locally

Install dependencies and run dev server using:

```
npm install
npm run
```

## TODOs

- handle back https://stackoverflow.com/questions/39342195/intercept-handle-browsers-back-button-in-react-router
- add Router https://reactrouter.com/en/main/start/overview
- (note that those seems to be VERY quirky, leave for last) add hotkeys (for inputs) https://github.com/jaywcjlove/react-hotkeys https://www.npmjs.com/package/react-keyboard-shortcuts
- consistent approach to private and non-private methods
- TODO semaphore suggestions
- TODO semaphore rotated (requires double-memory, as invalid flags may become valid)
- TODO pigpen T9 / square
