import React from 'react';
import ReactDOM from 'react-dom/client';
import Morse from './morse';
import './index.css';
import Menu from "./menu";
import {Cipher} from "./layout";
import menuData from "./menu_data";
import Braille from "./braille";
import LetterNumbers from "./letternumbers";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import Semaphore from "./semaphore";
import Substitution from "./substitution";
import Pigpen from "./pigpen";
import IcsBase from "./ics";


class Puzzler extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: '',
            menuVisible: true,
            darkMode: false,
        };
    }

    handleSelectView(code) {
        this.setState({
            view: code,
            menuVisible: false,
        });
    }

    onToggleMenu() {
        this.setState({
            menuVisible: !this.state.menuVisible,
        })
    }

    onToggleDarkMode() {
        this.refreshDarkMode(!this.state.darkMode);

        this.setState({
            darkMode: !this.state.darkMode
        });
    }

    refreshDarkMode(darkMode) {
        if (darkMode) {
            document.body.classList.add('dark');
        } else {
            document.body.classList.remove('dark');
        }
    }

    componentDidMount() {
        const state = onComponentDidMount(this, 'Puzzler');

        this.refreshDarkMode(state.darkMode);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Puzzler');
    }

    render() {

        let view;

        if (this.state.menuVisible) {
            view = '';
        } else {
            switch (this.state.view) {
                case 'morse':
                    view = <Morse title={menuData.get(this.state.view)}
                                  onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'braille':
                    view = <Braille title={menuData.get(this.state.view)}
                                    onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'letternumbers':
                    view = <LetterNumbers title={menuData.get(this.state.view)}
                                          onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'semaphore':
                    view = <Semaphore title={menuData.get(this.state.view)}
                                      onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'pigpen':
                    view = <Pigpen title={menuData.get(this.state.view)}
                                   onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'substitution':
                    view = <Substitution title={menuData.get(this.state.view)}
                                         onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                case 'ics':
                    view = <IcsBase title={menuData.get(this.state.view)}
                                    onToggleMenu={() => this.onToggleMenu()}/>
                    break;
                default:
                    view = (
                        <Cipher title={menuData.get(this.state.view)}
                                onToggleMenu={() => this.onToggleMenu()}
                                onTabChange={() => undefined}>
                            <h3>&#128679; Under construction &#128679;</h3>
                            <small>{this.state.view}</small>
                        </Cipher>
                    )
            }
        }

        return (
            <>
                <Menu visible={this.state.menuVisible}
                      menuData={menuData}
                      activeMenuKey={this.state.view}
                      onMenuSelect={(view) => this.handleSelectView(view)}/>
                {view}

                <hr/>

                <div>
                    <button onClick={() => this.onToggleDarkMode()}>
                        {this.state.darkMode ? 'Dark ON' : 'Dark OFF'}
                    </button>
                </div>
            </>
        );

    }
}

// ========================================

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Puzzler/>);
