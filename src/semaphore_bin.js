export default class SemaphoreImpl {
    static numbersFlag = '56';
    static lettersFlag = '57';
    static backspaceFlag = '48';
    static errorFlag = '99';

    static #codeToChar = new Map([
        ['12', 'A'],
        ['13', 'B'],
        ['14', 'C'],
        ['15', 'D'],
        ['16', 'E'],
        ['17', 'F'],
        ['18', 'G'],

        ['23', 'H'],
        ['24', 'I'],
        ['57', 'J'],
        ['25', 'K'],
        ['26', 'L'],
        ['27', 'M'],
        ['28', 'N'],

        ['34', 'O'],
        ['35', 'P'],
        ['36', 'Q'],
        ['37', 'R'],
        ['38', 'S'],

        ['45', 'T'],
        ['46', 'U'],

        ['58', 'V'],

        ['67', 'W'],
        ['68', 'X'],

        ['47', 'Y'],
        ['78', 'Z'],
    ]);
    static #codeToNum = new Map([
        ['12', '1'],
        ['13', '2'],
        ['14', '3'],
        ['15', '4'],
        ['16', '5'],
        ['17', '6'],
        ['18', '7'],

        ['23', '8'],
        ['24', '9'],
        ['25', '0'],
    ]);

    static #charToCode;
    static {
        SemaphoreImpl.#charToCode = new Map();

        let k, v;
        for ([k, v] of SemaphoreImpl.#codeToChar) {
            SemaphoreImpl.#charToCode.set(v, k);
        }

        for ([k, v] of SemaphoreImpl.#codeToNum) {
            SemaphoreImpl.#charToCode.set(v, k);
        }
    }

    textToPairs(text) {
        let numeric = false;
        let result = [];

        for (let c of text.split('')) {
            if (!SemaphoreImpl.#charToCode.has(c)) {
                continue;
            }

            if ('0123456789'.includes(c) && !numeric) {
                result.push(SemaphoreImpl.numbersFlag);
                numeric = true;
            } else if (!'0123456789'.includes(c) && numeric) {
                result.push(SemaphoreImpl.lettersFlag);
                numeric = false;
            }

            result.push(SemaphoreImpl.#charToCode.get(c));
        }
        return result;
    }

    isSwitchToNumbers(a, b, numericMode) {
        return !numericMode && this.#normalize(a, b) === SemaphoreImpl.numbersFlag;
    }

    isSwitchToLetters(a, b, numericMode) {
        return numericMode && this.#normalize(a, b) === SemaphoreImpl.lettersFlag;
    }

    toChar(a, b, isNumbers) {
        const key = this.#normalize(a, b);

        const map = isNumbers ? SemaphoreImpl.#codeToNum : SemaphoreImpl.#codeToChar;
        if (map.has(key)) {
            return map.get(key);
        }

        return '?';
    }

    charToCode(c) {
        return SemaphoreImpl.#charToCode.get(c);
    }

    sanitize(text) {
        let result = '';
        for (const c of text.toUpperCase().split('')) {
            if (SemaphoreImpl.#charToCode.has(c)) {
                result += c;
            }
        }
        return result;
    }

    #normalize(a, b) {
        return a < b ? ('' + a + b) : ('' + b + a);
    }
}
