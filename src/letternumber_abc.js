class BaseAlphabet {
    decToChar(asDec) {
        throw new Error('Not implemented');
    }

    charToDec(asChar) {
        throw new Error('Not implemented');
    }
}

export class A1Z26 extends BaseAlphabet {
    decToChar(asDec) {
        if (asDec < 1 || asDec > 26) {
            return '?'
        }

        return String.fromCharCode('A'.charCodeAt(0) + asDec - 1);
    }

    charToDec(asChar) {
        asChar = asChar.toUpperCase();

        const chr = asChar.charCodeAt(0) - 'A'.charCodeAt(0) + 1;
        if (chr >= 1 && chr <= 26) {
            return chr;
        }

        return '?';
    }
}

export class A1Z26Mod26 extends A1Z26 {
    decToChar(asDec) {
        if (asDec < 1) {
            return '?'
        }

        return String.fromCharCode('A'.charCodeAt(0) + ((asDec - 1) % 26));
    }
}

export class A0Z25 extends BaseAlphabet {
    decToChar(asDec) {
        if (asDec < 0 || asDec > 25) {
            return '?'
        }

        return String.fromCharCode('A'.charCodeAt(0) + asDec);
    }

    charToDec(asChar) {
        asChar = asChar.toUpperCase();

        const chr = asChar.charCodeAt(0) - 'A'.charCodeAt(0);
        if (chr >= 0 && chr <= 25) {
            return chr;
        }

        return '?';
    }
}

export class A0Z25Mod26 extends A0Z25 {
    decToChar(asDec) {
        if (asDec < 1) {
            return '?'
        }

        return String.fromCharCode('A'.charCodeAt(0) + (asDec % 26));
    }
}

export class Ascii extends BaseAlphabet {
    decToChar(asDec) {
        if (asDec < 0 || asDec > 127) {
            return '?'
        }

        return String.fromCharCode(asDec);
    }

    charToDec(asChar) {
        const chr = asChar.charCodeAt(0);
        if (chr >= 0 && chr <= 127) {
            return chr;
        }

        return '?';
    }
}

export default function alphabetByCode(code) {
    switch (code) {
        case 'a1z26':
            return new A1Z26();

        case 'a1z26mod26':
            return new A1Z26Mod26();

        case 'a0z25':
            return new A0Z25();

        case 'a0z25mod26':
            return new A0Z25Mod26();

        case 'ascii':
            return new Ascii();

        default:
            throw new Error("Illegal alphabet " + code);
    }
}
