import React from "react";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import {Cipher} from "./layout";
import {forceUpperAlpha} from "./alphabet";
import PigpenImpl from "./pigpen_bin";

const PIGPEN = new PigpenImpl();

function AsGraphicalPolybius5x5(props) {
    return (
        <>
            {props.inputPlaintext
                .split('')
                .map(
                    (c, i) => <React.Fragment key={i}>
                        {PIGPEN.polybius5x5CharToSvg(c, props.grid)}
                    </React.Fragment>
                )}
        </>
    );
}

function AsGraphicalPigpen(props) {
    switch (props.grid) {
        case '3x3x3-l-c-r':
        case '3x3x3-0-1-2':
            return (
                <>
                    {props.inputPlaintext
                        .replaceAll('CH', '#')
                        .split('')
                        .map(
                            (c, i) => <React.Fragment key={i}>
                                {PIGPEN.polishCharToSvg(c, props.grid, {})}
                            </React.Fragment>
                        )}
                </>
            );
        case '3x3+2x2-9-.-4':
        case '3x3+2x2-9-4-.':
        case '3x3+2x2-.-9-4':
            return (
                <>
                    {props.inputPlaintext
                        .split('')
                        .map(
                            (c, i) => <React.Fragment key={i}>
                                {PIGPEN.charToSvg(c, props.grid, {})}
                            </React.Fragment>
                        )}
                </>
            );
        default:
            throw new Error("Illegal grid " + props.grid);
    }
}

function AsGraphicalPolybius3x3(props) {
    return (
        <>
            {props.inputPlaintext
                .split('')
                .map(
                    (c, i) => <React.Fragment key={i}>
                        {PIGPEN.polybius3x3CharToSvg(c, props.grid)}
                    </React.Fragment>
                )}
        </>
    );
}


function AsPolybiusCoord(props) {
    return (
        props.inputPlaintext
            .split('')
            .map(c => PIGPEN.polybiusCoord(c, props.grid))
            .join(', ')
    );
}

function AsT9Taps(props) {
    return (
        props.inputPlaintext
            .split('')
            .map(
                c => {
                    const coord = PIGPEN.charToT9Coord(c);
                    return ('' + coord[0]).repeat(coord[1]);
                }
            )
            .join(', ')
    );
}

function AsT9Times(props) {
    return (
        props.inputPlaintext
            .split('')
            .map(
                c => {
                    const coord = PIGPEN.charToT9Coord(c);
                    return coord[0] + 'x' + coord[1];
                }
            )
            .join(', ')
    );
}

class PigpenEncrypt extends React.Component {
    constructor(props) {
        super(props);

        this.onInputPlaintextChange = this.onInputPlaintextChange.bind(this);
    }

    onInputPlaintextChange(event) {
        this.props.onInputPlaintextChange(forceUpperAlpha(event.target.value));
    }

    asCodes() {
        switch (this.props.grid) {
            case '3x3x3-l-c-r':
            case '3x3x3-0-1-2':
                return this.props.inputPlaintext
                    .replaceAll('CH', '#')
                    .split('')
                    .map(c => PIGPEN.polishCharToCode(c, this.props.grid))
                    .join(', ');

            case '3x3+2x2-9-.-4':
            case '3x3+2x2-9-4-.':
            case '3x3+2x2-.-9-4':
                return this.props.inputPlaintext
                    .split('')
                    .map(c => PIGPEN.charToCode(c, this.props.grid))
                    .join(', ');

            default:
                throw new Error("Illegal grid " + this.props.grid);
        }
    }

    render() {
        switch (this.props.grid) {
            case '3x3x3-l-c-r':
            case '3x3x3-0-1-2':
            case '3x3+2x2-9-.-4':
            case '3x3+2x2-9-4-.':
            case '3x3+2x2-.-9-4':
                const label = {
                    '3x3x3-l-c-r': <><code>3x3x3 L-C-R</code></>,
                    '3x3x3-0-1-2': <><code>3x3x3 0-1-2</code></>,
                    '3x3+2x2-9-.-4': <><code>3x3 + 2x2 9-&middot;-4</code> (A-I: <code>#</code>, J-R: <code>#&middot;</code>, S-V: <code>X</code>, W-Z: <code>X&middot;</code></>,
                    '3x3+2x2-9-4-.': <><code>3x3 + 2x2 9-4-&middot;</code> (A-I: <code>#</code>, N-V: <code>#&middot;</code>, J-M: <code>X</code>, W-Z: <code>X&middot;</code></>,
                    '3x3+2x2-.-9-4': <><code>3x3 + 2x2 &middot;-9-4</code> (A-V: alternating <code>#</code> and <code>#&middot;</code>, S-Z: alternating <code>X</code> and <code>X&middot;</code></>,
                }[this.props.grid];

                return (
                    <>
                        <div>
                            <label>Vstup:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onInputPlaintextChange}/>
                        </div>


                        <div>
                            <label>
                                Variant {label}
                            </label>

                            <label>Graphical:</label>
                            <div>
                                <AsGraphicalPigpen inputPlaintext={this.props.inputPlaintext}
                                                   grid={this.props.grid}/>
                            </div>
                            <label>Codes:</label>
                            <input type="text" className="monospace u-full-width" readOnly={true} value={this.asCodes()}/>
                        </div>

                    </>
                );
            case 'polybius-j':
            case 'polybius-q':
            case 'polybius-k':
            case 'polybius-x':
                return (
                    <>
                        <div>
                            <label>Vstup:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onInputPlaintextChange}/>
                        </div>


                        <div>
                            <label>
                                Variant {this.props.grid}
                            </label>

                            <label>5&times;5:</label>
                            <div>
                                <AsGraphicalPolybius5x5 inputPlaintext={this.props.inputPlaintext}
                                                        grid={this.props.grid}/>
                            </div>

                            <label>3&times;3:</label>
                            <div>
                                <AsGraphicalPolybius3x3 inputPlaintext={this.props.inputPlaintext}
                                                        grid={this.props.grid}/>
                            </div>

                            <label>Souřadnice (XY)</label>
                            <div>
                                <AsPolybiusCoord inputPlaintext={this.props.inputPlaintext}
                                                 grid={this.props.grid}/>
                            </div>
                        </div>

                    </>
                );
            case 't9':
                return (
                    <>
                        <div>
                            <label>Vstup:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onInputPlaintextChange}/>
                        </div>


                        <div>
                            <label>Multi-tap</label>
                            <div>
                                <AsT9Taps inputPlaintext={this.props.inputPlaintext}/>
                            </div>

                            <label>Klávesa + počet opakování</label>
                            <div>
                                <AsT9Times inputPlaintext={this.props.inputPlaintext}/>
                            </div>
                        </div>

                    </>
                );
            default:
                throw new Error("Illegal grid " + this.props.grid);
        }
    }
}

class PigpenDecrypt extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            t9letter: 0,
            t9count: 0,
        }
    }


    handleOnClick(c) {
        if (c === '') {
            return;
        }

        this.props.onInputPlaintextChange(
            this.props.inputPlaintext + c
        );
    }

    handleClickT9(i) {
        // 1 always submits
        if (i === 1) {
            this.submitCurrentT9Letter();
            return;
        }

        // blank always creates new line
        if (this.state.t9count === 0) {
            this.setState({
                t9count: 1,
                t9letter: i,
            });

            return;
        }

        // same letter always tries to increase
        if (this.state.t9letter === i) {
            const maxCount = this.state.t9letter !== 7 && this.state.t9letter !== 9 ? 3 : 4;


            this.setState({
                t9count: (this.state.t9count % maxCount) + 1,
            });

            return;
        }

        // different letter first submits, then creates new line
        this.submitCurrentT9Letter();
        this.setState({
            t9count: 1,
            t9letter: i,
        });
    }

    submitCurrentT9Letter() {
        if (this.state.t9count === 0) {
            return;
        }

        const letter = PigpenImpl.ALPHABET_BY_VARIANT['t9'][this.state.t9letter - 1][this.state.t9count - 1];

        this.props.onInputPlaintextChange(
            this.props.inputPlaintext + letter,
        );

        this.setState({
            t9letter: '',
            t9count: 0,
        });
    }

    backspace() {
        const inputPlaintext = this.props.inputPlaintext;
        if (inputPlaintext.length) {
            this.props.onInputPlaintextChange(inputPlaintext.slice(0, inputPlaintext.length - 1));
        }
    }

    clear() {
        this.props.onInputPlaintextChange('');
    }

    #keyboard() {
        switch (this.props.grid) {
            case '3x3x3-l-c-r':
            case '3x3x3-0-1-2':
                return this.#keyboardPolish();

            case '3x3+2x2-9-.-4':
            case '3x3+2x2-9-4-.':
            case '3x3+2x2-.-9-4':
                return this.#keyboardPigpen();

            default:
                return (<>Under construction</>);
        }
    }

    #keyboardPigpen() {
        const alpha = PigpenImpl.ALPHABET_BY_VARIANT[this.props.grid].split('');
        const squareChunks = [
            alpha.slice(0, 3).concat(alpha.slice(9, 12)),
            alpha.slice(3, 6).concat(alpha.slice(12, 15)),
            alpha.slice(6, 9).concat(alpha.slice(15, 18)),
        ];
        const wedgeChunks = [
            ['', alpha[18], '', '', alpha[22], ''],
            [alpha[19], '', alpha[20], alpha[23], '', alpha[24]],
            ['', alpha[21], '', '', alpha[25], ''],
        ]

        return (
            <table className="borderless compact u-full-width cursor-pointer">
                <tbody>

                {
                    squareChunks.map(
                        cccccc => (
                            <React.Fragment key={cccccc}>
                                <tr>
                                    {cccccc.map(
                                        c => (
                                            <React.Fragment key={c}>
                                                <td onClick={() => this.handleOnClick(c)}>
                                                    {PIGPEN.charToSvg(c, this.props.grid, {
                                                        width: '100%',
                                                        height: '100%',
                                                    })}
                                                </td>
                                                <td onClick={() => this.handleOnClick(c)} className="muted small">{c}</td>
                                            </React.Fragment>
                                        )
                                    )}
                                </tr>
                            </React.Fragment>
                        )
                    )
                }

                {
                    wedgeChunks.map(
                        cccccc => (
                            <React.Fragment key={cccccc}>
                                <tr className="p-0">
                                </tr>
                                <tr>
                                    {cccccc.map(
                                        (c, i) => (
                                            <React.Fragment key={(c === '' ? i : c)}>
                                                <td onClick={() => this.handleOnClick(c)}>
                                                    {c === '' ? '' : PIGPEN.charToSvg(c, this.props.grid, {
                                                        width: '100%',
                                                        height: '100%',
                                                    })}
                                                </td>
                                                <td onClick={() => this.handleOnClick(c)} className="small muted">{c}</td>
                                            </React.Fragment>
                                        )
                                    )}
                                </tr>
                            </React.Fragment>
                        )
                    )
                }

                </tbody>
            </table>
        );
    }

    #keyboardPolish() {
        const alpha = 'ABCDEFGH#IJKLMNOPQRSTUVWXYZ'.split('');
        const chunks = [
            alpha.slice(0, 9),
            alpha.slice(9, 18),
            alpha.slice(18, 27),
        ];

        return (
            <table className="borderless compact u-full-width tl-fixed cursor-pointer">
                <tbody>

                {
                    chunks.map(
                        ccc => (
                            <React.Fragment key={ccc}>
                                <tr className="p-0">
                                    {ccc.map(
                                        c => (
                                            <td key={c} className="text-center muted" onClick={() => this.handleOnClick(c)}>{c === '#' ? 'CH' : c}</td>
                                        )
                                    )}
                                </tr>
                                <tr>
                                    {ccc.map(
                                        c => (
                                            <td key={c + "p"} onClick={() => this.handleOnClick(c)}>
                                                {PIGPEN.polishCharToSvg(c, this.props.grid, {
                                                    width: '100%',
                                                    height: '100%',
                                                })}
                                            </td>
                                        )
                                    )}
                                </tr>
                            </React.Fragment>
                        )
                    )
                }

                </tbody>
            </table>
        );
    }

    #keyboardPolybius() {

        const alpha = PigpenImpl.ALPHABET_BY_VARIANT[this.props.grid].split('');
        const squareChunks = [
            alpha.slice(0, 5),
            alpha.slice(5, 10),
            alpha.slice(10, 15),
            alpha.slice(15, 20),
            alpha.slice(20, 25),
        ];

        return (
            <table className="borderless compact u-full-width cursor-pointer">
                <tbody>
                <tr>
                    <td className="py-5p text-center text-muted">&nbsp;</td>
                    <td className="py-5p text-center text-muted">1</td>
                    <td className="py-5p text-center text-muted">2</td>
                    <td className="py-5p text-center text-muted">3</td>
                    <td className="py-5p text-center text-muted">4</td>
                    <td className="py-5p text-center text-muted">5</td>
                </tr>

                {
                    squareChunks.map(
                        (ccccc, rowId) => (
                            <React.Fragment key={rowId}>
                                <tr>
                                    <td className="py-5p text-center text-muted">{rowId + 1}</td>
                                    {ccccc.map(
                                        (c, colId) => {
                                            let style = {};

                                            if (rowId === 0) {
                                                style['borderTop'] = '2px solid black';
                                            }
                                            if (colId === 0) {
                                                style['borderLeft'] = '2px solid black';
                                            }
                                            if (colId === 4) {
                                                style['borderRight'] = '2px solid black';
                                            }
                                            if (rowId === 4) {
                                                style['borderBottom'] = '2px solid black';
                                            }
                                            if (rowId === 1 && colId > 0 && colId < 4) {
                                                style['borderTop'] = '1px solid black';
                                            }
                                            if (rowId === 3 && colId > 0 && colId < 4) {
                                                style['borderBottom'] = '1px solid black';
                                            }
                                            if (colId === 1 && rowId > 0 && rowId < 4) {
                                                style['borderLeft'] = '1px solid black';
                                            }
                                            if (colId === 3 && rowId > 0 && rowId < 4) {
                                                style['borderRight'] = '1px solid black';
                                            }

                                            return (
                                                <td className="py-5p text-center" key={c} onClick={() => this.handleOnClick(c)} style={style}>
                                                    {c}
                                                </td>
                                            );
                                        }
                                    )}
                                </tr>
                            </React.Fragment>
                        )
                    )
                }

                </tbody>
            </table>
        );
    }

    #keyboardT9() {
        const keypad = [
            PigpenImpl.ALPHABET_BY_VARIANT['t9'].slice(0, 3),
            PigpenImpl.ALPHABET_BY_VARIANT['t9'].slice(3, 6),
            PigpenImpl.ALPHABET_BY_VARIANT['t9'].slice(6, 9),
        ];

        return (
            <table className="border-1 compact u-full-width cursor-pointer">
                <tbody>
                {
                    keypad.map(
                        (row, rowId) => (
                            <tr key={rowId}>
                                {row.map(
                                    (chars, colId) => (
                                        <React.Fragment key={colId}>
                                            <th onClick={() => this.handleClickT9(3 * rowId + colId + 1)} className="text-center">{3 * rowId + colId + 1}</th>
                                            <td onClick={() => this.handleClickT9(3 * rowId + colId + 1)} className="text-center">
                                                {chars.map((c, letterId) => {
                                                    return (
                                                        <span key={letterId}
                                                              className={(this.state.t9letter === 3 * rowId + colId + 1 && this.state.t9count === letterId + 1) ? 't9 highlight' : 't9'}>
                                                            {c}
                                                        </span>
                                                    );
                                                })}
                                            </td>
                                        </React.Fragment>
                                    )
                                )}
                            </tr>
                        )
                    )
                }

                </tbody>
            </table>
        );
    }

    render() {
        switch (this.props.grid) {
            case 't9':
                return (
                    <>
                        <div>
                            {this.#keyboardT9()}
                        </div>

                        <table className="u-full-width">
                            <tbody>
                            <tr>
                                <td>
                                    <button onClick={() => this.submitCurrentT9Letter()}>&#x27A4;</button>
                                </td>
                                <td>
                                    <button onClick={() => this.backspace()}>&#x232B;</button>
                                </td>
                                <td>
                                    <button onClick={() => this.clear()}>&#x1F5D1;</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div>
                            <label>Text:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} readOnly={true}/>
                        </div>
                    </>
                );

            case '3x3x3-l-c-r':
            case '3x3x3-0-1-2':
            case '3x3+2x2-9-.-4':
            case '3x3+2x2-9-4-.':
            case '3x3+2x2-.-9-4':
                return (
                    <>
                        <div>
                            {this.#keyboard()}
                        </div>

                        <table className="u-full-width">
                            <tbody>
                            <tr>
                                <td>
                                    <button onClick={() => this.backspace()}>&#x232B;</button>
                                </td>
                                <td>
                                    <button onClick={() => this.clear()}>&#x1F5D1;</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div>
                            <label>Text:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} readOnly={true}/>
                        </div>

                        <div>
                            <label>Graphical:</label>
                            <div>
                                <AsGraphicalPigpen inputPlaintext={this.props.inputPlaintext}
                                                   grid={this.props.grid}/>
                            </div>
                        </div>
                    </>
                );
            case 'polybius-j':
            case 'polybius-k':
            case 'polybius-q':
            case 'polybius-x':
                return (
                    <>
                        <div>
                            {this.#keyboardPolybius()}
                        </div>

                        <table className="u-full-width">
                            <tbody>
                            <tr>
                                <td>
                                    <button onClick={() => this.backspace()}>&#x232B;</button>
                                </td>
                                <td>
                                    <button onClick={() => this.clear()}>&#x1F5D1;</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div>
                            <label>Text:</label>
                            <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} readOnly={true}/>
                        </div>

                        <label>5&times;5:</label>
                        <div>
                            <AsGraphicalPolybius5x5 inputPlaintext={this.props.inputPlaintext}
                                                    grid={this.props.grid}/>
                        </div>

                        <label>3&times;3:</label>
                        <div>
                            <AsGraphicalPolybius3x3 inputPlaintext={this.props.inputPlaintext}
                                                    grid={this.props.grid}/>
                        </div>

                        <label>Souřadnice (XY)</label>
                        <div>
                            <AsPolybiusCoord inputPlaintext={this.props.inputPlaintext}
                                             grid={this.props.grid}/>
                        </div>
                    </>
                );

            default:
                throw new Error('Illegal grid ' + this.props.grid);
        }
    }
}

class Pigpen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',

            grid: '3x3x3-l-c-r',
            inputPlaintext: '',
        };

        this.onGridChange = this.onGridChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'Pigpen');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Pigpen');
    }

    onGridChange(event) {
        this.setState({
            grid: event.target.value,
        });
    }

    onInputPlaintextChange(inputPlaintext) {
        this.setState({
            inputPlaintext: inputPlaintext,
        })
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {

            case 'encrypt':
                view = <PigpenEncrypt inputPlaintext={this.state.inputPlaintext}
                                      grid={this.state.grid}
                                      onInputPlaintextChange={(p) => this.onInputPlaintextChange(p)}/>
                break;

            case 'decrypt':
                view = <PigpenDecrypt inputPlaintext={this.state.inputPlaintext}
                                      grid={this.state.grid}
                                      onInputPlaintextChange={(p) => this.onInputPlaintextChange(p)}/>
                break;

            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}
                    hideReference={true}>
                <div>
                    <label htmlFor="grid">Grid</label>
                    <select id="grid" value={this.state.grid} onChange={this.onGridChange}>
                        <option value="3x3x3-l-c-r">Pigpen 3x3x3 (L-C-R)</option>
                        <option value="3x3x3-0-1-2">Pigpen 3x3x3 (0-1-2)</option>
                        <option value="3x3+2x2-9-.-4">Pigpen 3x3+2x2 (9-.-4)</option>
                        <option value="3x3+2x2-9-4-.">Pigpen 3x3+2x2 (9-4-.)</option>
                        <option value="3x3+2x2-.-9-4">Pigpen 3x3+2x2 (.-9-4)</option>
                        <option value="polybius-j">Polybius (&minus;J)</option>
                        <option value="polybius-k">Polybius (&minus;K)</option>
                        <option value="polybius-q">Polybius (&minus;Q)</option>
                        <option value="polybius-x">Polybius (&minus;X)</option>
                        <option value="t9">Multi-tap / T9</option>
                    </select>
                </div>
                {view}
            </Cipher>
        )
    }
}

export default Pigpen;
