import React from "react";
import {Cipher} from './layout'
import morseCode from "./morse_data";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";

function MorseReference() {
    return (
        <table>
            <tbody>
            {morseCode.filter((morse) => morse[0] !== ' ').map((morse) => (
                <tr key={morse[0]}>
                    <td>{morse[0]}</td>
                    <td className="monospace">{morse[1]}</td>
                    <td>{morse[2]}</td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}

class MorseEncrypt extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.handleChange(event.target.value);
    }

    render() {
        return (
            <>
                <div>
                    <label>Input:</label>
                    <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onChange}/>
                </div>

                <div>
                    <label>Direct:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asMorse}/>
                </div>

                <div>
                    <label>1 = ., 2 = -, 0 = /</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asMorse120}/>
                </div>

                <div>
                    <label>Binary signal</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asBinarySignal}/>
                </div>
            </>
        )
    }
}

class MorseDecrypt extends React.Component {
    constructor(props) {
        super(props);


        this.onChange = this.onChange.bind(this);
    }

    addMorse(char) {
        this.props.handleChange(this.props.inputMorse + char);
    }

    backspace() {
        const morse = this.props.inputMorse;
        if (morse.length) {
            this.props.handleChange(morse.slice(0, morse.length - 1));
        }
    }

    clear() {
        this.props.handleChange('');
    }

    onChange(event) {
        this.props.handleChange(event.target.value.replaceAll(/[^-./]/g, ''));
    }

    render() {
        return (
            <>
                <div>
                    <button className="mr-1" onClick={() => this.addMorse('.')}>&nbsp;&middot;&nbsp;</button>
                    <button className="mr-1" onClick={() => this.addMorse('-')}>&mdash;</button>
                    <button className="mr-1" onClick={() => this.addMorse('/')}>/</button>
                </div>
                <div>
                    <button className="mr-1" onClick={() => this.backspace()}>&#x232B;</button>
                    <button className="mr-1" onClick={() => this.clear()}>&#x1F5D1;</button>
                </div>

                <div>
                    <label>Input</label>
                    <input type="text" className="monospace u-full-width" value={this.props.inputMorse} onChange={this.onChange}/>
                </div>

                <div>
                    <label>Direct</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asPlainText}/>
                </div>

                <div>
                    <label>Switched dots and dashes</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asPlaintextSwitched}/>
                </div>

                <div>
                    <label>Read in reverse</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asPlaintextReversed}/>
                </div>

                <div>
                    <label>Reversed + switched</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asPlaintextSwitchedReversed}/>
                </div>

            </>
        )
    }
}

class Morse extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',

            inputPlaintext: '',
            inputMorse: '',

            asMorse: '',
            asMorse120: '',
            asBinarySignal: '',

            asPlaintext: '',
            asPlaintextSwitched: '',
            asPlaintextReversed: '',
            asPlaintextSwitchedReversed: '',
        };

    }

    handleChangeEncrypt(inputPlaintext) {
        inputPlaintext = inputPlaintext.toUpperCase();

        this.propagateEncryptDecrypt(
            Morse.#encode(inputPlaintext),
            inputPlaintext
        );
    }

    handleChangeDecrypt(inputMorse) {
        // TODO consistency? Propagate each click, or just submissions?
        this.propagateEncryptDecrypt(
            inputMorse,
            Morse.#decode(inputMorse)
        );
    }

    propagateEncryptDecrypt(inputMorse, inputPlaintext) {
        const switched = inputMorse.replaceAll('.', 'x').replaceAll('-', '.').replaceAll('x', '-');

        const asMorse120 = inputMorse.replaceAll('.', '1').replaceAll('-', '2').replaceAll('/', '0');

        let asBinarySignal = '';
        let prevPause = true;
        for (let m of inputMorse.split('')) {
            switch (m) {
                case '.':
                    if (!prevPause) {
                        asBinarySignal += '0';
                    }
                    asBinarySignal += '1';
                    prevPause = false;
                    break;
                case '-':
                    if (!prevPause) {
                        asBinarySignal += '0';
                    }
                    asBinarySignal += '111';
                    prevPause = false;
                    break;
                case '/':
                    asBinarySignal += '000';
                    prevPause = true;
                    break;
                default:
                    throw new Error("Illegal character " + m);
            }
        }

        this.setState({
            inputMorse: inputMorse,
            inputPlaintext: inputPlaintext,

            asPlaintext: inputPlaintext,
            asPlaintextSwitched: Morse.#decode(switched),
            asPlaintextReversed: Morse.#decode(inputMorse.split('').reverse().join('')),
            asPlaintextSwitchedReversed: Morse.#decode(switched.split('').reverse().join('')),

            asMorse: inputMorse,
            asMorse120: asMorse120,
            asBinarySignal: asBinarySignal,
        })
    }

    componentDidMount() {
        onComponentDidMount(this, 'Morse');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Morse');
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {
            case 'encrypt':
                view = <MorseEncrypt inputPlaintext={this.state.inputPlaintext}
                                     asMorse={this.state.asMorse}
                                     asMorse120={this.state.asMorse120}
                                     asBinarySignal={this.state.asBinarySignal}
                                     handleChange={(p) => this.handleChangeEncrypt(p)}/>
                break;

            case 'decrypt':
                view = <MorseDecrypt inputMorse={this.state.inputMorse}
                                     asPlainText={this.state.asPlaintext}
                                     asPlaintextSwitched={this.state.asPlaintextSwitched}
                                     asPlaintextReversed={this.state.asPlaintextReversed}
                                     asPlaintextSwitchedReversed={this.state.asPlaintextSwitchedReversed}
                                     handleChange={(m) => this.handleChangeDecrypt(m)}/>
                break;

            case 'reference':
                view = <MorseReference/>
                break;
            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}>
                {view}
            </Cipher>
        )
    }

    static #plainToMorse;
    static #morseToPlain;
    static {
        this.#plainToMorse = {};
        this.#morseToPlain = {};

        for (let morse of morseCode) {
            this.#plainToMorse[morse[0]] = morse[1];
            this.#morseToPlain[morse[1]] = morse[0];
        }
    }

    static #decode(morse) {
        return morse
            .split('/')
            .map(
                c => c in Morse.#morseToPlain ? Morse.#morseToPlain[c] : '?'
            )
            .join('');
    }

    static #encode(plaintext) {
        return plaintext
            .toUpperCase()
            .split('')
            .filter(c => c in Morse.#plainToMorse)
            .map(
                c => Morse.#plainToMorse[c]
            )
            .join('/');
    }
}

export default Morse;
