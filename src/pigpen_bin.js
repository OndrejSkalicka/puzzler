import React from "react";

export default class PigpenImpl {
    static #PIGPEN_VERTICES = {
        'a': '2 2',
        'b': '18 2',
        'c': '2 18',
        'd': '18 18',
        'e': '10 2',
        'f': '2 10',
        'g': '18 10',
        'h': '10 18',
    }

    static #PIGPEN_WEDGE_DOTS = {
        0: [10, 8],
        1: [8, 10],
        2: [12, 10],
        3: [10, 12],
    }

    static #PIGPEN_POLISH_VERTICES = {
        'a': '2 2',
        'b': '28 2',
        'c': '2 18',
        'd': '28 18',
    }

    static #PIGPEN_LINES = {
        0: 'cdb',
        1: 'acdb',
        2: 'acd',
        3: 'abdc',
        4: 'abdca',
        5: 'bacd',
        6: 'abd',
        7: 'cabd',
        8: 'cab',
    }

    static #PIGPEN_WEDGE_LINES = {
        0: 'ahb',
        1: 'agc',
        2: 'bfd',
        3: 'ced',
    }

    static #PIGPEN_CODES = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',

        '1*',
        '2*',
        '3*',
        '4*',
        '5*',
        '6*',
        '7*',
        '8*',
        '9*',

        'A',
        'B',
        'C',
        'D',

        'A*',
        'B*',
        'C*',
        'D*',
    ]

    static ALPHABET_BY_VARIANT = {
        '3x3+2x2-9-.-4': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        '3x3+2x2-9-4-.': 'ABCDEFGHINOPQRSTUVJKLMWXYZ',
        '3x3+2x2-.-9-4': 'ACEGIKMOQBDFHJLNPRSUWYTVXZ',
        'polybius-j': 'ABCDEFGHIKLMNOPQRSTUVWXYZ',
        'polybius-q': 'ABCDEFGHIJKLMNOPRSTUVWXYZ',
        'polybius-k': 'ABCDEFGHIJLMNOPQRSTUVWXYZ',
        'polybius-x': 'ABCDEFGHIJKLMNOPQRSTUVWYZ',
        't9': [
            [], ['A', 'B', 'C'], ['D', 'E', 'F'],
            ['G', 'H', 'I'], ['J', 'K', 'L'], ['M', 'N', 'O'],
            ['P', 'Q', 'R', 'S'], ['T', 'U', 'V'], ['W', 'X', 'Y', 'Z'],
        ]
    }

    charToT9Coord(c) {
        for (const [blockId, block] of PigpenImpl.ALPHABET_BY_VARIANT.t9.entries()) {
            if (!block.includes(c)) {
                continue;
            }

            return [blockId + 1, block.indexOf(c) + 1];
        }

        throw new Error('Cannot find ' + c + ' in ' + PigpenImpl.ALPHABET_BY_VARIANT.t9);
    }

    charToSvg(char, variant, props) {
        const index = this.#charToPigpenIndex(char, variant);

        if (index < 18) {
            const lines = PigpenImpl.#PIGPEN_LINES[Math.floor(index % 9)];
            const path = 'M ' + (
                lines.split('')
                    .map(c => PigpenImpl.#PIGPEN_VERTICES[c])
                    .join(' L ')
            );

            let circle;

            if (index < 9) {
                circle = '';
            } else {
                circle = <circle cx="10" cy="10" r="2"/>;
            }

            return (
                <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                    <path d={path} fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                    {circle}
                </svg>
            );
        } else {
            const wedgeIndex = index - 18;

            const lines = PigpenImpl.#PIGPEN_WEDGE_LINES[Math.floor(wedgeIndex % 4)];
            const path = 'M ' + (
                lines.split('')
                    .map(c => PigpenImpl.#PIGPEN_VERTICES[c])
                    .join(' L ')
            );

            let circle;
            if (wedgeIndex < 4) {
                circle = '';
            } else {
                circle = <circle cx={PigpenImpl.#PIGPEN_WEDGE_DOTS[wedgeIndex % 4][0]} cy={PigpenImpl.#PIGPEN_WEDGE_DOTS[wedgeIndex % 4][1]} r="2"/>;
            }

            return (
                <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                    <path d={path} fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                    {circle}
                </svg>
            );
        }
    }

    charToCode(char, variant) {
        return PigpenImpl.#PIGPEN_CODES[this.#charToPigpenIndex(char, variant)];
    }


    #charToPigpenIndex(char, variant) {
        const alpha = PigpenImpl.ALPHABET_BY_VARIANT[variant];

        return alpha.indexOf(char);
    }

    polishCharToSvg(char, variant, props) {
        const index = 'ABCDEFGH#IJKLMNOPQRSTUVWXYZ'.indexOf(char);

        if (variant === '3x3x3-l-c-r') {
            return this.#polishLCR(index, props);
        } else {
            return this.#polish012(index, props);
        }
    }

    polishCharToCode(char, variant) {
        const index = 'ABCDEFGH#IJKLMNOPQRSTUVWXYZ'.indexOf(char);

        if (variant === '3x3x3-l-c-r') {
            return '' + (Math.floor(index / 3) + 1) + ['L', 'C', 'R'][index % 3];
        } else {
            return '' + (Math.floor(index / 3) + 1) + '*'.repeat(index % 3);
        }
    }

    #polishLCR(letterNumber, props) {
        const lines = PigpenImpl.#PIGPEN_LINES[Math.floor(letterNumber / 3)];
        const path = 'M ' + (
            lines.split('')
                .map(c => PigpenImpl.#PIGPEN_POLISH_VERTICES[c])
                .join(' L ')
        );

        let circle;
        if (letterNumber % 3 === 0) {
            circle = <circle cx="6" cy="10" r="1.5"/>;
        } else if (letterNumber % 3 === 1) {
            circle = <circle cx="15" cy="10" r="1.5"/>;
        } else {
            circle = <circle cx="24" cy="10" r="1.5"/>;
        }

        return (
            <svg height="20" width="30" viewBox="0 0 30 20" {...props}>
                <path d={path} fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                {circle}
            </svg>
        );
    }

    #polish012(letterNumber, props) {
        const lines = PigpenImpl.#PIGPEN_LINES[Math.floor(letterNumber / 3)];
        const path = 'M ' + (
            lines.split('')
                .map(c => PigpenImpl.#PIGPEN_POLISH_VERTICES[c])
                .join(' L ')
        );

        let circle;
        if (letterNumber % 3 === 0) {
            circle = '';
        } else if (letterNumber % 3 === 1) {
            circle = <circle cx="15" cy="10" r="1.5"/>;
        } else {
            circle = (
                <>
                    <circle cx="10" cy="10" r="1.5"/>
                    <circle cx="20" cy="10" r="1.5"/>
                </>
            );
        }

        return (
            <svg height="20" width="30" viewBox="0 0 30 20" {...props}>
                <path d={path} fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                {circle}
            </svg>
        );
    }

    polybius5x5CharToSvg(char, variant, props) {
        const index = this.#charToPigpenIndex(char, variant);

        if (index < 0) {
            return (
                <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                    <rect x="2" y="2" width="16" height="16" strokeWidth="1" fill="none" stroke="#000000"/>
                    <path d="M 4 4 L 16 16 M 4 16 L 16 4" fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                </svg>
            );
        }

        return (
            <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                <rect x="2" y="2" width="16" height="16" strokeWidth="1" fill="none" stroke="#000000"/>
                <rect x={2.5 + (index % 5) * 3} y={2.5 + Math.floor(index / 5) * 3} width="3" height="3" fill="#000000"/>
            </svg>
        );
    }

    polybius3x3CharToSvg(char, variant, props) {
        const index = this.#charToPigpenIndex(char, variant);

        if (index < 0) {
            return (
                <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                    <rect x="5.5" y="5.5" width="9" height="9" strokeWidth="1" fill="none" stroke="#000000"/>
                    <path d="M 4 4 L 16 16 M 4 16 L 16 4" fill="none" stroke="#000000" strokeLinecap="round" strokeWidth="2"/>
                </svg>
            );
        }

        let x = 2.5 + (index % 5) * 3;
        let y = 2.5 + Math.floor(index / 5) * 3;

        if (x === 5.5) {
            x = 5;
        }
        if (x === 11.5) {
            x = 12;
        }
        if (y === 5.5) {
            y = 5;
        }
        if (y === 11.5) {
            y = 12;
        }
        return (
            <svg height="30" width="30" viewBox="0 0 20 20" {...props}>
                <rect x="5.5" y="5.5" width="9" height="9" strokeWidth="1" fill="none" stroke="#000000"/>
                <rect x={x} y={y} width="3" height="3" fill="#000000"/>
            </svg>
        );
    }

    polybiusCoord(char, variant) {
        const index = this.#charToPigpenIndex(char, variant);

        if (index < 0) {
            return '?';
        }

        return '' + ((index % 5) + 1) + (Math.floor(index / 5) + 1);
    }
}
