export function onComponentDidMount(target, key) {
    try {
        const state = window.localStorage.getItem(key);
        if (state !== null) {
            const stateJson = JSON.parse(state);
            target.setState(stateJson);

            return stateJson;
        }
        return {};
    } catch (e) {

    }
}

export function onComponentDidUpdate(target, key) {
    window.localStorage.setItem(key, JSON.stringify(target.state));
}
