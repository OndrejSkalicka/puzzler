import React from "react";
import {Cipher} from './layout'
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import SemaphoreImpl from "./semaphore_bin";
import {semaphoreByCode} from "./svg";

class SemaphoreReference extends React.Component {
    static #semaphore = new SemaphoreImpl();

    render() {
        return (
            <table className="compact borderless">
                <tbody>
                <tr>
                    <th colSpan="8">Písmena</th>
                </tr>
                {['ABCD', 'EFGH', 'IJKL', 'MNOP', 'QRST', 'UVWX', 'YZ']
                    .map(cccc => (
                        <tr key={cccc}>
                            {[...cccc]
                                .map(c => (
                                    <React.Fragment key={c}>
                                        <td>{semaphoreByCode(SemaphoreReference.#semaphore.charToCode(c))}</td>
                                        <td>{c}</td>
                                    </React.Fragment>
                                ))}
                        </tr>
                    ))
                }
                <tr>
                    <th colSpan="8">Číslice</th>
                </tr>
                {['1234', '5678', '90']
                    .map(cccc => (
                        <tr key={cccc}>
                            {[...cccc]
                                .map(c => (
                                    <React.Fragment key={c}>
                                        <td>{semaphoreByCode(SemaphoreReference.#semaphore.charToCode(c))}</td>
                                        <td>{c}</td>
                                    </React.Fragment>
                                ))}
                        </tr>
                    ))
                }
                <tr>
                    <th colSpan="8">Řídící znaky</th>
                </tr>
                <tr>
                    <td>{semaphoreByCode(SemaphoreImpl.numbersFlag)}</td>
                    <td colSpan="3">Číslice</td>
                    <td>{semaphoreByCode(SemaphoreImpl.lettersFlag)}</td>
                    <td colSpan="3">Písmena</td>
                </tr>
                <tr>
                    <td>{semaphoreByCode(SemaphoreImpl.backspaceFlag)}</td>
                    <td colSpan="3">Smazat poslední</td>
                    <td>{semaphoreByCode(SemaphoreImpl.errorFlag)}</td>
                    <td colSpan="3">Chyba, Pozor</td>
                </tr>
                </tbody>
            </table>
        );
    }
}

class SemaphoreEncrypt extends React.Component {
    static #semaphore = new SemaphoreImpl();

    constructor(props) {
        super(props);

        this.state = {
            a1z26Expanded: true,
            a0z25Expanded: false,
            asciiExpanded: false,
        }

        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'SemaphoreEncrypt');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'SemaphoreEncrypt');
    }

    onChange(event) {
        this.props.handleChange(SemaphoreEncrypt.#semaphore.sanitize(event.target.value));
    }

    asFlags() {
        let result = [];
        const pairs = SemaphoreEncrypt.#semaphore.textToPairs(this.props.inputPlaintext);

        for (const pair of pairs) {
            result.push(semaphoreByCode(pair, {key: '' + result.length}));
        }

        return result;
    }

    asPairs() {
        return SemaphoreEncrypt.#semaphore.textToPairs(this.props.inputPlaintext).join(', ');
    }


    render() {
        return (
            <>
                <div>
                    <label>Vstup:</label>
                    <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onChange}/>
                </div>

                <div>
                    <label>Praporky:</label>
                    <div>
                        {this.asFlags()}
                    </div>
                </div>
                <div>
                    <label>Dvojice (1 = dolů, poté po směru ručiček):</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.asPairs()}/>
                </div>
            </>
        )
    }
}

class SemaphoreDecrypt extends React.Component {
    static #semaphore = new SemaphoreImpl();

    constructor(props) {
        super(props);

        this.state = {
            activeButton: null,
            numericMode: false,
        };
    }

    componentDidMount() {
        onComponentDidMount(this, 'SemaphoreDecrypt');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'SemaphoreDecrypt');
    }

    backspace() {
        if (this.state.activeButton !== null) {
            this.setState({
                activeButton: null,
            });
        } else if (this.props.inputPlaintext.length > 0) {
            // try to delete last letter
            this.props.handleChange(this.props.inputPlaintext.slice(0, this.props.inputPlaintext.length - 1));

        }
    }

    clear() {
        if (this.state.activeButton !== null) {
            this.setState({
                activeButton: null,
            });
        } else {
            this.props.handleChange('');
        }
    }

    isActive(input) {
        return this.state.activeButton === input;
    }

    toggleNumericMode() {
        this.setState({
            numericMode: !this.state.numericMode,
        });
    }

    toggleActive(input) {
        const a = this.state.activeButton;
        const b = input;
        const numericMode = this.state.numericMode;

        if (a === b) {
            this.setState({
                activeButton: null,
            });

            return;
        }

        if (a === null) {
            this.setState({
                activeButton: b,
            });
            return;
        }

        const semaphore = new SemaphoreImpl();
        if (semaphore.isSwitchToNumbers(a, b, numericMode)) {
            this.setState({
                activeButton: null,
                numericMode: true,
            });
            return;
        }

        if (semaphore.isSwitchToLetters(a, b, numericMode)) {
            this.setState({
                activeButton: null,
                numericMode: false,
            });

            return;
        }


        this.setState({
            activeButton: null,
        })
        this.props.handleChange(
            this.props.inputPlaintext + semaphore.toChar(a, b, numericMode),
        );
    }

    asFlags() {
        let result = [];
        const pairs = SemaphoreDecrypt.#semaphore.textToPairs(this.props.inputPlaintext);

        for (const pair of pairs) {
            result.push(semaphoreByCode(pair, {key: '' + result.length}));
        }

        return result;
    }

    render() {
        return (
            <>

                <table className="borderless compact">
                    <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(5) ? 'button-primary' : ''} onClick={() => this.toggleActive(5)}>5</button>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(4) ? 'button-primary' : ''} onClick={() => this.toggleActive(4)}>4</button>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(6) ? 'button-primary' : ''} onClick={() => this.toggleActive(6)}>6</button>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <button className={this.isActive(3) ? 'button-primary' : ''} onClick={() => this.toggleActive(3)}>3</button>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <svg width='70' height='50' viewBox='221.785 101.694 70 215'>
                                <g
                                    fill='#000'
                                    fillOpacity='1'
                                    fillRule='evenodd'
                                    stroke='none'
                                    strokeDasharray='none'
                                    strokeMiterlimit='4'
                                    strokeOpacity='1'
                                    strokeWidth='2.5'
                                    transform='translate(-43.215 -83.306)'
                                >
                                    <path
                                        d='M275 200a25 25 0 11-50 0 25 25 0 1150 0z'
                                        opacity='1'
                                        transform='translate(50 10)'
                                    ></path>
                                    <rect
                                        width='70'
                                        height='160'
                                        x='265'
                                        y='240'
                                        opacity='1'
                                        rx='20'
                                        ry='21.333'
                                    ></rect>
                                </g>
                            </svg>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(7) ? 'button-primary' : ''} onClick={() => this.toggleActive(7)}>7</button>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(2) ? 'button-primary' : ''} onClick={() => this.toggleActive(2)}>2</button>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(8) ? 'button-primary' : ''} onClick={() => this.toggleActive(8)}>8</button>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <button className={this.isActive(1) ? 'button-primary' : ''} onClick={() => this.toggleActive(1)}>1</button>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

                <table className="u-full-width">
                    <tbody>
                    <tr>
                        <td>
                            <button className={this.state.numericMode ? 'button-primary' : ''} onClick={() => this.toggleNumericMode()}>Num</button>
                        </td>
                        <td>
                            <button onClick={() => this.backspace()}>&#x232B;</button>
                        </td>
                        <td>
                            <button onClick={() => this.clear()}>&#x1F5D1;</button>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <label>Praporky:</label>
                    <div>
                        {this.asFlags()}
                    </div>
                </div>

                <div>
                    <label>Text:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.inputPlaintext}/>
                </div>
            </>
        )
    }
}

class Semaphore extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',

            inputPlaintext: '',
        };
    }

    handleChangeInput(inputPlaintext) {
        this.setState({
            inputPlaintext: inputPlaintext,
        })
    }

    componentDidMount() {
        onComponentDidMount(this, 'Semaphore');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Semaphore');
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {

            case 'encrypt':
                view = <SemaphoreEncrypt inputPlaintext={this.state.inputPlaintext}
                                         handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'decrypt':
                view = <SemaphoreDecrypt inputPlaintext={this.state.inputPlaintext}
                                         handleChange={(m) => this.handleChangeInput(m)}/>
                break;

            case 'reference':
                view = <SemaphoreReference/>
                break;
            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}>
                {view}
            </Cipher>
        )
    }
}

export default Semaphore;
