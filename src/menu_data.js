const menuData = new Map([
    ['morse', 'Morseovka'],
    ['braille', 'Braillovo písmo'],
    ['letternumbers', 'Čísla písmen'],
    ['semaphore', 'Semaforová abeceda'],
    ['pigpen', 'Polský kříž, šifrovací tabulky'],
    ['ics', 'Vlajková abeceda'],
    ['substitution', 'Substituce a posuny'],
    ['transposition', 'Transpoziční šifry 🚧'],
    ['frequency', 'Frekvenční analýza 🚧'],
    ['calendar', 'Kalendář, svátky 🚧'],
    ['notes', 'Zápisník stanovišť 🚧'],
    ['words', 'Databáze slov 🚧'],
]);

export default menuData;
