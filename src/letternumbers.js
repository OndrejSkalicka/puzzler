import React from "react";
import {Cipher} from './layout'
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import alphabetByCode from "./letternumber_abc";
import baseByCode from "./letternumber_base";

class LetterNumbersReference extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alphabet: 'a1z26',
        };

        this.onAlphabetChange = this.onAlphabetChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'LetterNumbersReference');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'LetterNumbersReference');
    }

    onAlphabetChange(event) {
        this.setState({
            alphabet: event.target.value,
        });
    }

    render() {
        const alphabet = alphabetByCode(this.state.alphabet);
        const baseB2 = baseByCode('b2');
        const baseB3 = baseByCode('b3');
        const baseB8 = baseByCode('b8');
        const baseB16 = baseByCode('b16');

        let reference;

        if (this.state.alphabet === 'ascii') {
            reference = (
                <table className="u-full-width">
                    <thead>
                    <tr>
                        <th>Lt</th>
                        <th>Dec</th>
                        <th>bin</th>
                        <th>ter</th>
                        <th>oct</th>
                        <th>hex</th>
                    </tr>
                    </thead>
                    <tbody>
                    {[...'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz']
                        .map(c => (
                            <tr key={c}>
                                <td>{c}</td>
                                <td>{alphabet.charToDec(c)}</td>
                                <td>{baseB2.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB3.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB8.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB16.decToBase(alphabet.charToDec(c))}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            reference = (
                <table className="u-full-width">
                    <thead>
                    <tr>
                        <th>Lt</th>
                        <th>Dec</th>
                        <th>bin</th>
                        <th>ter</th>
                        <th>oct</th>
                        <th>hex</th>
                    </tr>
                    </thead>
                    <tbody>
                    {[...'ABCDEFGHIJKLMNOPQRSTUVWXYZ']
                        .map(c => (
                            <tr key={c}>
                                <td>{c}</td>
                                <td>{alphabet.charToDec(c)}</td>
                                <td>{baseB2.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB3.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB8.decToBase(alphabet.charToDec(c))}</td>
                                <td>{baseB16.decToBase(alphabet.charToDec(c))}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        }

        return (
            <>
                <div>
                    <label htmlFor="alphabet">Alphabet</label>
                    <select id="alphabet" value={this.state.alphabet} onChange={this.onAlphabetChange}>
                        <option value="a1z26">A=1, Z=26</option>
                        <option value="a0z25">A=0, Z=25</option>
                        <option value="ascii">ASCII</option>
                    </select>
                </div>

                {reference}
            </>
        );
    }
}

class LetterNumbersEncrypt extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a1z26Expanded: true,
            a0z25Expanded: false,
            asciiExpanded: false,
        }

        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'LetterNumbersEncrypt');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'LetterNumbersEncrypt');
    }

    onChange(event) {
        this.props.handleChange(event.target.value);
    }

    encodedAs(alphabetCode, baseCode) {
        let result = [];
        const alphabet = alphabetByCode(alphabetCode);
        const base = baseByCode(baseCode);

        for (let c of this.props.inputPlaintext.split('')) {
            const asDec = alphabet.charToDec(c);
            const asBase = asDec === '?' ? '?' : base.decToBase(asDec);
            result.push(asBase);
        }
        return result.join(', ');
    }


    render() {
        return (
            <>
                <div>
                    <label>Vstup:</label>
                    <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onChange}/>
                </div>

                <h5 onClick={() => this.setState({a1z26Expanded: !this.state.a1z26Expanded})}>Číslování od 1 {this.state.a1z26Expanded ? '+' : '-'}</h5>
                <div hidden={!this.state.a1z26Expanded}>
                    <div>
                        <label>Binárně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'b2')}/>
                    </div>
                    <div>
                        <label>Ternárně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'b3')}/>
                    </div>
                    <div>
                        <label>Oktálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'b8')}/>
                    </div>
                    <div>
                        <label>Decimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'b10')}/>
                    </div>
                    <div>
                        <label>Hexadecimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'b16')}/>
                    </div>
                    <div>
                        <label>Římsky:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'roman')}/>
                    </div>
                    <div>
                        <label>Permutace (bez Q, X):</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a1z26', 'perm')}/>
                    </div>
                </div>

                <h5 onClick={() => this.setState({a0z25Expanded: !this.state.a0z25Expanded})}>Číslování od 0 {this.state.a0z25Expanded ? '+' : '-'}</h5>
                <div hidden={!this.state.a0z25Expanded}>
                    <div>
                        <label>Binárně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'b2')}/>
                    </div>
                    <div>
                        <label>Ternárně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'b3')}/>
                    </div>
                    <div>
                        <label>Oktálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'b8')}/>
                    </div>
                    <div>
                        <label>Decimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'b10')}/>
                    </div>
                    <div>
                        <label>Hexadecimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'b16')}/>
                    </div>
                    <div>
                        <label>Římsky:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'roman')}/>
                    </div>
                    <div>
                        <label>Permutace (bez Q, X):</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('a0z25', 'perm')}/>
                    </div>
                </div>

                <h5 onClick={() => this.setState({asciiExpanded: !this.state.asciiExpanded})}>ASCII {this.state.asciiExpanded ? '+' : '-'}</h5>
                <div hidden={!this.state.asciiExpanded}>
                    <div>
                        <label>Binárně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('ascii', 'b2')}/>
                    </div>
                    <div>
                        <label>Oktálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('ascii', 'b8')}/>
                    </div>
                    <div>
                        <label>Decimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('ascii', 'b10')}/>
                    </div>
                    <div>
                        <label>Hexadecimálně:</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.encodedAs('ascii', 'b16')}/>
                    </div>
                </div>
            </>
        )
    }
}

class LetterNumbersDecrypt extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            base: 'b10',
            alphabet: 'a1z26',
            activeInput: '',
        };

        this.onBaseChange = this.onBaseChange.bind(this);
        this.onAlphabetChange = this.onAlphabetChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'LetterNumbersDecrypt');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'LetterNumbersDecrypt');
    }

    addInput(c) {
        this.setState({
            activeInput: this.state.activeInput + c,
        })
    }

    backspace() {
        const activeInput = this.state.activeInput;

        if (activeInput.length === 0) {
            // try to delete last letter
            if (this.props.inputPlaintext.length > 0) {
                this.props.handleChange(this.props.inputPlaintext.slice(0, this.props.inputPlaintext.length - 1));
                return;
            }
        }

        if (activeInput.length > 0) {
            this.setState({
                activeInput: activeInput.slice(0, activeInput.length - 1),
            });
        }
    }

    clear() {
        if (this.state.activeInput.length > 0) {
            this.setState({
                activeInput: '',
            });
        } else {
            this.props.handleChange('');
        }
    }

    submitLetter() {
        this.setState({
            activeInput: '',
        });
        this.props.handleChange(
            this.props.inputPlaintext + this.#decToChar(this.#toDec(this.state.activeInput)),
        );
    }

    inputDisplay() {
        if (this.state.activeInput === '') {
            return <>&nbsp;</>;
        }

        let result = '';
        let asDec = this.#toDec(this.state.activeInput);
        let ignoreModSuffix = false;

        switch (this.state.base) {
            case 'b2':
                result += this.state.activeInput + ' (base 2) = ' + asDec + " (base 10) = ";
                break;
            case 'b3':
                result += this.state.activeInput + ' (base 3) = ' + asDec + " (base 10) = ";
                break;
            case 'b8':
                result += this.state.activeInput + ' (base 8) = ' + asDec + " (base 10) = ";
                break;
            case 'b10':
                result += this.state.activeInput + ' (base 10) = ';
                break;
            case 'b16':
                result += this.state.activeInput + ' (base 16) = ' + asDec + " (base 10) = ";
                break;
            case 'roman':
                result += this.state.activeInput + ' (roman) = ' + asDec + " (base 10) = ";
                break;
            case 'perm':
                result += this.state.activeInput + ' (perm) = ' + asDec + " (base 10) = ";
                ignoreModSuffix = true;
                break;
            case 'perm_inverse':
                result += this.state.activeInput + ' (i-perm) = ' + asDec + " (base 10) = ";
                ignoreModSuffix = true;
                break;
            default:
                throw new Error("Illegal base " + this.state.base);
        }

        if (!ignoreModSuffix) {
            if (this.state.alphabet === 'a1z26mod26') {
                result += (((asDec - 1) % 26) + 1) + " (mod 26*) = ";
            }

            if (this.state.alphabet === 'a0z25mod26') {
                result += (asDec % 26) + " (mod 26) = ";
            }
        }

        result += this.#decToChar(asDec);

        return result;
    }

    #toDec(input) {
        return baseByCode(this.state.base).inputToDec(input);
    }

    #decToChar(asDec) {
        return alphabetByCode(this.state.alphabet).decToChar(asDec);
    }

    onBaseChange(event) {
        this.setState({
            activeInput: '',
            base: event.target.value,
        });
    }

    onAlphabetChange(event) {
        this.setState({
            alphabet: event.target.value,
        });
    }

    #keyboard() {
        switch (this.state.base) {
            case 'b2':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('0')}>0</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('1')}>1</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'b3':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('0')}>0</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('1')}>1</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('2')}>2</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'b8':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('0')}>0</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('1')}>1</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('2')}>2</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('3')}>3</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('4')}>4</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('5')}>5</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('6')}>6</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('7')}>7</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'b10':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('1')}>1</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('2')}>2</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('3')}>3</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('4')}>4</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('5')}>5</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('6')}>6</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('7')}>7</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('8')}>8</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('9')}>9</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <button onClick={() => this.addInput('0')}>0</button>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'b16':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('0')}>0</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('1')}>1</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('2')}>2</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('3')}>3</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('4')}>4</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('5')}>5</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('6')}>6</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('7')}>7</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('8')}>8</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('9')}>9</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('A')}>A</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('B')}>B</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('C')}>C</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('D')}>D</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('E')}>E</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('F')}>F</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'roman':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('I')}>I</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('V')}>V</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('X')}>X</button>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('L')}>L</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('C')}>C</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('D')}>D</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('M')}>M</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            case 'perm':
            case 'perm_inverse':
                return (
                    <table className="borderless compact">
                        <tbody>
                        <tr>
                            <td>
                                <button onClick={() => this.addInput('A')} disabled={this.state.activeInput.includes('A')}>A</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('B')} disabled={this.state.activeInput.includes('B')}>B</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('C')} disabled={this.state.activeInput.includes('C')}>C</button>
                            </td>
                            <td>
                                <button onClick={() => this.addInput('D')} disabled={this.state.activeInput.includes('D')}>D</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                )

            default:
                return <>Under construction</>
        }
    }

    render() {
        return (
            <>
                <table className="u-full-width borderless">
                    <tbody>
                    <tr>
                        <td>
                            <label htmlFor="base">Base</label>
                            <select id="base" value={this.state.base} onChange={this.onBaseChange}>
                                <option value="b2">Binary</option>
                                <option value="b3">Ternary</option>
                                <option value="b8">Octal</option>
                                <option value="b10">Decimal</option>
                                <option value="b16">Hexadecimal</option>
                                <option value="roman">Roman numerals</option>
                                <option value="perm">Permutations (bez Q, X)</option>
                                <option value="perm_inverse" disabled={true}>Inverse permutations</option>
                            </select>
                        </td>
                        <td>
                            <label htmlFor="alphabet">Alphabet</label>
                            <select id="alphabet" disabled={['perm', 'perm_inverse'].includes(this.state.base)} value={this.state.alphabet}
                                    onChange={this.onAlphabetChange}>
                                <option value="a1z26">A=1, Z=26</option>
                                <option value="a0z25">A=0, Z=25</option>
                                <option value="a1z26mod26">A=1, Z=26 mod 26</option>
                                <option value="a0z25mod26">A=0, Z=25 mod 26</option>
                                <option value="ascii">ASCII</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>

                {this.#keyboard()}

                <table className="u-full-width">
                    <tbody>
                    <tr>
                        <td colSpan="3" className="monospace" onClick={() => this.submitLetter()}>{this.inputDisplay()}</td>
                    </tr>
                    <tr>
                        <td>
                            <button onClick={() => this.submitLetter()}>&#x27A4;</button>
                        </td>
                        <td>
                            <button onClick={() => this.backspace()}>&#x232B;</button>
                        </td>
                        <td>
                            <button onClick={() => this.clear()}>&#x1F5D1;</button>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <label>Text:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.inputPlaintext}/>
                </div>
            </>
        )
    }
}

class LetterNumbers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',

            inputPlaintext: '',
        };
    }

    handleChangeInput(inputPlaintext) {
        this.setState({
            inputPlaintext: inputPlaintext,
        })
    }

    componentDidMount() {
        onComponentDidMount(this, 'LetterNumbers');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'LetterNumbers');
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {

            case 'encrypt':
                view = <LetterNumbersEncrypt inputPlaintext={this.state.inputPlaintext}
                                             handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'decrypt':
                view = <LetterNumbersDecrypt inputPlaintext={this.state.inputPlaintext}
                                             handleChange={(m) => this.handleChangeInput(m)}/>
                break;

            case 'reference':
                view = <LetterNumbersReference/>
                break;
            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}>
                {view}
            </Cipher>
        )
    }
}

export default LetterNumbers;
