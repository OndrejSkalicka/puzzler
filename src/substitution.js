import React from "react";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import {alpha, forceUpperAlpha} from "./alphabet";

class Caesar extends React.Component {

    shifted(i) {
        return [...this.props.inputPlaintext]
            .map(c => this.#shiftedChar(c, i))
            .join('');
    }

    #shiftedChar(c, i) {
        return alpha[(alpha.indexOf(c) + i) % alpha.length];
    }

    render() {
        return (
            <>
                {[...Array(26).keys()].map(i => (
                    <div key={i}>
                        <label className="muted">A &rarr; {this.#shiftedChar('A', i)} (+{i} / &minus;{(26 - i) % 26})</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.shifted(i)}/>
                    </div>
                ))}
            </>
        );
    }
}

class Atbash extends React.Component {

    shifted(i) {
        return [...this.props.inputPlaintext]
            .map(c => this.#shiftedChar(c, i))
            .join('');
    }

    #shiftedChar(c, i) {
        return alpha[(alpha.length - alpha.indexOf(c) - i - 1 + alpha.length) % alpha.length];
    }

    render() {
        return (
            <>
                {[...Array(26).keys()].map(i => (
                    <div key={i}>
                        <label className="muted">A &rarr; {this.#shiftedChar('A', i)} (+{i} / &minus;{(26 - i) % 26})</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.shifted(i)}/>
                    </div>
                ))}
            </>
        );
    }
}

class Affine extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a: '3',
        }

        this.onAChange = this.onAChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'Affine');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Affine');
    }

    onAChange(event) {
        this.setState({
            a: event.target.value,
        });
    }


    shifted(i) {
        return [...this.props.inputPlaintext]
            .map(c => this.#shiftedChar(c, i))
            .join('');
    }

    #shiftedChar(c, i) {
        return alpha[(alpha.indexOf(c) * parseInt(this.state.a) + i) % alpha.length];
    }

    render() {
        return (
            <>

                <div>
                    <label htmlFor="a">A*X + B</label>
                    <select id="a" className="u-full-width" value={this.state.a} onChange={this.onAChange}>
                        <option value="3">A = 3</option>
                        <option value="5">A = 5</option>
                        <option value="7">A = 7</option>
                        <option value="9">A = 9</option>
                        <option value="11">A = 11</option>
                        <option value="15">A = 15</option>
                        <option value="17">A = 17</option>
                        <option value="19">A = 19</option>
                        <option value="21">A = 21</option>
                        <option value="23">A = 23</option>
                    </select>
                </div>

                {[...Array(26).keys()].map(i => (
                    <div key={i}>
                        <label className="muted">A = {this.state.a}, B = {i}</label>
                        <input type="text" className="monospace u-full-width" readOnly={true} value={this.shifted(i)}/>
                    </div>
                ))}
            </>
        );
    }
}

class Vigenere extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: '',
        }

        this.onKeywordChange = this.onKeywordChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'Vigenere');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Vigenere');
    }

    onKeywordChange(event) {
        this.setState({
            keyword: forceUpperAlpha(event.target.value),
        });
    }

    #decypher(callback) {
        const text = this.props.inputPlaintext;
        const keyword = this.state.keyword;

        if (keyword.length === 0) {
            return text;
        }

        let result = '';

        for (let i = 0; i < text.length; ++i) {
            const c = text[i];

            const an = alpha.indexOf(c);
            const bn = alpha.indexOf(keyword[i % keyword.length]);
            const ban = i < keyword.length ? bn : alpha.indexOf(text[i - keyword.length]);
            const bav = i < keyword.length ? bn : alpha.indexOf(result[i - keyword.length]);

            const indexRaw = callback(an, bn, ban, bav);
            const indexModulo = ((indexRaw % alpha.length) + alpha.length) % alpha.length;
            result += alpha[indexModulo];
        }

        return result;
    }

    render() {
        return (
            <>

                <div>
                    <label>Keyword:</label>
                    <input type="text" className="monospace u-full-width" value={this.state.keyword} onChange={this.onKeywordChange}/>
                </div>

                <hr/>

                <p className="muted">
                    n = index písmene ve výsledku. A(n) písmeno v Ciphertext. B(n) písmeno v Keyword (keyword se opakuje pořád dokola). [BA](n) písmeno
                    v Keyword, za keyword přidán Ciphertext. [BV](n) písmeno v Keyword, za keyword přidán výsledek.
                </p>

                <div>
                    <label className="muted">A(n) + B(n) (od 1) (Vigenère)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => an + bn + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - B(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => an - bn - 1)}/>
                </div>

                <div>
                    <label className="muted">B(n) - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => bn - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + B(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => an + bn)}/>
                </div>

                <div>
                    <label className="muted">A(n) - B(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => an - bn)}/>
                </div>

                <div>
                    <label className="muted">B(n) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => bn - an)}/>
                </div>

                <div>
                    <label className="muted">A(n) + [BA](n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => an + ban + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - [BA](n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => an - ban - 1)}/>
                </div>

                <div>
                    <label className="muted">[BA](n) - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => ban - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + [BA](n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => an + ban)}/>
                </div>

                <div>
                    <label className="muted">A(n) - [BA](n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => an - ban)}/>
                </div>

                <div>
                    <label className="muted">[BA](n) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban) => ban - an)}/>
                </div>

                <div>
                    <label className="muted">A(n) + [BV](n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => an + bv + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - [BV](n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => an - bv - 1)}/>
                </div>

                <div>
                    <label className="muted">[BV](n) - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => bv - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + [BV](n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => an + bv)}/>
                </div>

                <div>
                    <label className="muted">A(n) - [BV](n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => an - bv)}/>
                </div>

                <div>
                    <label className="muted">[BV](n) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn, ban, bv) => bv - an)}/>
                </div>

                <div>
                    <label className="muted">2A(n) - B(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => 2 * an - bn)}/>
                </div>

                <div>
                    <label className="muted">2B(n) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, bn) => 2 * bn - an)}/>
                </div>
            </>
        );
    }
}

class Autokey extends React.Component {
    #decypher(callback) {
        const text = this.props.inputPlaintext;

        if (text.length === 0) {
            return '';
        }

        let result = text[0];

        for (let i = 1; i < text.length; ++i) {
            const c = text[i];

            const an = alpha.indexOf(c);
            const an1 = alpha.indexOf(text[i - 1]);
            const vn1 = alpha.indexOf(result[i - 1]);

            const indexRaw = callback(an, an1, vn1);
            const indexModulo = ((indexRaw % alpha.length) + alpha.length) % alpha.length;
            result += alpha[indexModulo];
        }

        return result;
    }

    render() {
        return (
            <>
                <p className="muted">
                    n = index písmene ve výsledku. A(n) písmeno v Ciphertext. V(n) písmeno ve výsledku.
                </p>

                <div>
                    <label className="muted">A(n) + A(n-1) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an + an1 + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - A(n-1) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an - an1 - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n-1) - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an1 - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + A(n-1) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an + an1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - A(n-1) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an - an1)}/>
                </div>

                <div>
                    <label className="muted">A(n-1) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => an1 - an)}/>
                </div>

                <div>
                    <label className="muted">A(n) + V(n-1) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => an + vn1 + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - V(n-1) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => an - vn1 - 1)}/>
                </div>

                <div>
                    <label className="muted">V(n-1) - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => vn1 - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + V(n-1) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => an + vn1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - V(n-1) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => an - vn1)}/>
                </div>

                <div>
                    <label className="muted">V(n-1) - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1, vn1) => vn1 - an)}/>
                </div>

                <div>
                    <label className="muted">2A(n) - A(n-1) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, an1) => 2 * an - an1)}/>
                </div>
            </>
        );
    }
}

class KeyPosition extends React.Component {
    #decypher(callback) {
        const text = this.props.inputPlaintext;

        if (text.length === 0) {
            return '';
        }

        let result = '';

        for (let i = 0; i < text.length; ++i) {
            const c = text[i];

            const an = alpha.indexOf(c);

            const indexRaw = callback(an, i);
            const indexModulo = ((indexRaw % alpha.length) + alpha.length) % alpha.length;
            result += alpha[indexModulo];
        }

        return result;
    }

    render() {
        return (
            <>
                <p className="muted">
                    n = index písmene ve výsledku. A(n) písmeno v Ciphertext. V(n) písmeno ve výsledku.
                </p>

                <div>
                    <label className="muted">A(n) + n (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => an + n + 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) - n (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => an - n - 1)}/>
                </div>

                <div>
                    <label className="muted">n - A(n) (od 1)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => n - an - 1)}/>
                </div>

                <div>
                    <label className="muted">A(n) + n (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => an + n)}/>
                </div>

                <div>
                    <label className="muted">A(n) - n (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => an - n)}/>
                </div>

                <div>
                    <label className="muted">n - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => n - an)}/>
                </div>

                <div>
                    <label className="muted">2A(n) - n (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => 2 * an - n)}/>
                </div>

                <div>
                    <label className="muted">2n - A(n) (od 0)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.#decypher((an, n) => 2 * n - an)}/>
                </div>
            </>
        );
    }
}

class Substitution extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            algorithm: 'caesar',

            inputPlaintext: '',
        };

        this.onAlgorithmChange = this.onAlgorithmChange.bind(this);
        this.onInputPlaintextChange = this.onInputPlaintextChange.bind(this);
    }

    componentDidMount() {
        onComponentDidMount(this, 'Substitution');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Substitution');
    }

    onAlgorithmChange(event) {
        this.setState({
            algorithm: event.target.value,
        });
    }

    onInputPlaintextChange(event) {
        this.setState({
            inputPlaintext: forceUpperAlpha(event.target.value),
        });
    }

    render() {
        let view;
        switch (this.state.algorithm) {

            case 'caesar':
                view = <Caesar inputPlaintext={this.state.inputPlaintext}
                               handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'atbash':
                view = <Atbash inputPlaintext={this.state.inputPlaintext}
                               handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'affine':
                view = <Affine inputPlaintext={this.state.inputPlaintext}
                               handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'vignere':
                view = <Vigenere inputPlaintext={this.state.inputPlaintext}
                                 handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'autokey':
                view = <Autokey inputPlaintext={this.state.inputPlaintext}
                                handleChange={(p) => this.handleChangeInput(p)}/>
                break;

            case 'key_position':
                view = <KeyPosition inputPlaintext={this.state.inputPlaintext}
                                    handleChange={(p) => this.handleChangeInput(p)}/>
                break;
            default:
                view = <div>Illegal view {this.state.algorithm}</div>
        }

        return (
            <>
                <h2>{this.props.title}
                    <button className="u-pull-right" onClick={this.props.onToggleMenu}>&equiv;</button>
                </h2>
                <hr/>

                <div>
                    <label htmlFor="algorithm">Typ šifry</label>
                    <select id="algorithm" value={this.state.algorithm} onChange={this.onAlgorithmChange} className="u-full-width">
                        <option value="caesar">Shifts (Caesar)</option>
                        <option value="atbash">Reverse and shifts (Atbash)</option>
                        <option value="affine">Affine ciphers</option>
                        <option value="vignere">Polyalphabetic (Vigenère etc.)</option>
                        <option value="autokey">Autokey ciphers</option>
                        <option value="key_position">Key = position</option>
                    </select>
                </div>

                <div>
                    <label>Ciphertext:</label>
                    <input type="text" className="monospace u-full-width" value={this.state.inputPlaintext} onChange={this.onInputPlaintextChange}/>
                </div>

                {view}
            </>
        )
    }
}

export default Substitution;
