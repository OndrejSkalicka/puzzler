import React from "react";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";
import {Cipher} from "./layout";
import {icsByChar} from "./svg";


class IcsReference extends React.Component {

    render() {
        return (
            <table className="compact borderless">
                <tbody>
                <tr>
                    <th colSpan="8">Písmena</th>
                </tr>
                {['ABCD', 'EFGH', 'IJKL', 'MNOP', 'QRST', 'UVWX', 'YZ']
                    .map(cccc => (
                        <tr key={cccc}>
                            {[...cccc]
                                .map(c => (
                                    <React.Fragment key={c}>
                                        <td>{icsByChar(c, {})}</td>
                                        <td>{c}</td>
                                    </React.Fragment>
                                ))}
                        </tr>
                    ))
                }
                <tr>
                    <th colSpan="8">Číslice</th>
                </tr>
                {['1234', '5678', '90']
                    .map(cccc => (
                        <tr key={cccc}>
                            {[...cccc]
                                .map(c => (
                                    <React.Fragment key={c}>
                                        <td>{icsByChar(c)}</td>
                                        <td>{c}</td>
                                    </React.Fragment>
                                ))}
                        </tr>
                    ))
                }
                </tbody>
            </table>
        )
    }
}

class IcsEncrypt extends React.Component {
    render() {
        return (
            <div>Encrypt</div>
        )
    }
}

class IcsDecrypt extends React.Component {
    render() {
        return (
            <div>Decrypt</div>
        )
    }
}

class IcsBase extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',
            inputPlaintext: '',
        };

    }

    componentDidMount() {
        onComponentDidMount(this, 'IcsBase');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'IcsBase');
    }

    onInputPlaintextChange(inputPlaintext) {
        this.setState({
            inputPlaintext: inputPlaintext,
        })
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {

            case 'encrypt':
                view = <IcsEncrypt inputPlaintext={this.state.inputPlaintext}
                                   grid={this.state.grid}
                                   onInputPlaintextChange={(p) => this.onInputPlaintextChange(p)}/>
                break;

            case 'decrypt':
                view = <IcsDecrypt inputPlaintext={this.state.inputPlaintext}
                                   grid={this.state.grid}
                                   onInputPlaintextChange={(p) => this.onInputPlaintextChange(p)}/>
                break;

            case 'reference':
                view = <IcsReference/>
                break;
            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}>
                {view}
            </Cipher>
        )
    }
}

export default IcsBase;
