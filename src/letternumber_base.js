// noinspection JSValidateTypes,JSCheckFunctionSignatures

class BaseBase {
    decToBase(asDec) {
        throw new Error('Not implemented');
    }

    inputToDec(asStr) {
        throw new Error('Not implemented');
    }
}

class Binary extends BaseBase {

    decToBase(asDec) {
        return asDec.toString(2).padStart(5, '0');
    }

    inputToDec(asStr) {
        return parseInt(asStr, 2);
    }
}

class Ternary extends BaseBase {

    decToBase(asDec) {
        return asDec.toString(3).padStart(3, '0');
    }

    inputToDec(asStr) {
        return parseInt(asStr, 3);
    }
}

class Octal extends BaseBase {

    decToBase(asDec) {
        return asDec.toString(8).padStart(2, '0');
    }

    inputToDec(asStr) {
        return parseInt(asStr, 8);
    }
}

class Decimal extends BaseBase {

    decToBase(asDec) {
        return asDec.toString();
    }

    inputToDec(asStr) {
        return parseInt(asStr);
    }
}

class Hexadecimal extends BaseBase {

    decToBase(asDec) {
        return asDec.toString(16).padStart(2, '0').toUpperCase();
    }

    inputToDec(asStr) {
        return parseInt(asStr, 16);
    }
}

class Roman extends BaseBase {

    decToBase(asDec) {
        if (!+asDec) return false;
        let digits = String(+asDec).split('');
        let key = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM',
            '', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC',
            '', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
        let roman = '', i = 3;
        while (i--) roman = (key[+digits.pop() + (i * 10)] || '') + roman;
        return Array(+digits.join('') + 1).join('M') + roman;
    }

    inputToDec(asStr) {
        // see https://blog.stevenlevithan.com/archives/javascript-roman-numeral-converter
        let str = asStr.toUpperCase();
        let validator = /^M*(?:D?C{0,3}|C[MD])(?:L?X{0,3}|X[CL])(?:V?I{0,3}|I[XV])$/;
        let token = /[MDLV]|C[MD]?|X[CL]?|I[XV]?/g;
        let key = {M: 1000, CM: 900, D: 500, CD: 400, C: 100, XC: 90, L: 50, XL: 40, X: 10, IX: 9, V: 5, IV: 4, I: 1};
        let num = 0, m;
        if (!(str && validator.test(str))) return 0;
        while (true) {
            m = token.exec(str);
            if (!m) {
                break;
            }
            num += key[m[0]];
        }
        return num;
    }
}

class Permutation extends BaseBase {
    static #inputToDec = {
        'ABCD': 1, // A
        'ABDC': 2, // B
        'ACBD': 3, // C
        'ACDB': 4, // D
        'ADBC': 5, // E
        'ADCB': 6, // F
        'BACD': 7, // G
        'BADC': 8, // H
        'BCAD': 9, // I
        'BCDA': 10, // J
        'BDAC': 11, // K
        'BDCA': 12, // L
        'CABD': 13, // M
        'CADB': 14, // N
        'CBAD': 15, // O
        'CBDA': 16, // P
        'CDAB': 18, // R
        'CDBA': 19, // S
        'DABC': 20, // T
        'DACB': 21, // U
        'DBAC': 22, // V
        'DBCA': 23, // X
        'DCAB': 24, // Y
        'DCBA': 26, // Z
    }
    static #decToInput = {
        1: 'ABCD', // A
        2: 'ABDC', // B
        3: 'ACBD', // C
        4: 'ACDB', // D
        5: 'ADBC', // E
        6: 'ADCB', // F
        7: 'BACD', // G
        8: 'BADC', // H
        9: 'BCAD', // I
        10: 'BCDA', // J
        11: 'BDAC', // K
        12: 'BDCA', // L
        13: 'CABD', // M
        14: 'CADB', // N
        15: 'CBAD', // O
        16: 'CBDA', // P
        18: 'CDAB', // R
        19: 'CDBA', // S
        20: 'DABC', // T
        21: 'DACB', // U
        22: 'DBAC', // V
        23: 'DBCA', // X
        25: 'DCAB', // Y
        26: 'DCBA', // Z
    }


    decToBase(asDec) {
        if (asDec in Permutation.#decToInput) {
            return Permutation.#decToInput[asDec];
        }

        return '?';
    }

    inputToDec(asStr) {
        if (asStr in Permutation.#inputToDec) {
            return Permutation.#inputToDec[asStr];
        }

        return 0;
    }
}

class PermutationInverse extends BaseBase {

    decToBase(asDec) {
        super.decToBase(asDec);
    }

    inputToDec(asStr) {
        return 0;
    }
}

export default function baseByCode(code) {
    switch (code) {
        case 'b2':
            return new Binary();
        case 'b3':
            return new Ternary();
        case 'b8':
            return new Octal();
        case 'b10':
            return new Decimal();
        case 'b16':
            return new Hexadecimal();
        case 'roman':
            return new Roman();
        case 'perm':
            return new Permutation();
        case 'perm_inverse':
            return new PermutationInverse();
        default:
            throw new Error("Illegal base " + code);
    }
}

