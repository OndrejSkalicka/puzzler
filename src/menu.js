import React from 'react';

function MenuItem(props) {
    return (
        <div>
            <button className={props.activeMenuKey === props.menuKey ? 'u-full-width active' : 'u-full-width'}
                    onClick={() => props.onMenuSelect(props.menuKey)}>{props.title}</button>
        </div>
    );
}

class Menu extends React.Component {
    render() {
        if (!this.props.visible) {
            return null;
        }

        return (
            <>
                {Array.from(this.props.menuData, ([menuKey, label]) => (
                    <MenuItem key={menuKey}
                              menuKey={menuKey}
                              activeMenuKey={this.props.activeMenuKey}
                              title={label}
                              onMenuSelect={this.props.onMenuSelect}/>
                ))}
            </>
        )
    }
}

export default Menu;
