import React from "react";
import {Cipher} from './layout'
import brailleCode, {binToGlyph} from "./braille_data";
import {onComponentDidMount, onComponentDidUpdate} from "./local_storage";

function BrailleReference() {
    return (
        <table>
            <tbody>
            {brailleCode.filter((morse) => morse[0] !== ' ').map((morse) => (
                <tr key={morse[0]}>
                    <td>{morse[0]}</td>
                    <td><code>{morse[1]}</code></td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}

class BrailleEncrypt extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.handleChange(event.target.value);
    }

    render() {
        return (
            <>
                <div>
                    <label>Vstup:</label>
                    <input type="text" className="monospace u-full-width" value={this.props.inputPlaintext} onChange={this.onChange}/>
                </div>

                <div>
                    <label>Graficky:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asGlyphs}/>
                </div>

                <div>
                    <label>Číselné označení</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asNumbers}/>
                </div>

                <div>
                    <label>Binárně (123456)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asBinary}/>
                </div>

                <div>
                    <label>Decimálně (1-2-4-8-16-32)</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asDecimal}/>
                </div>
            </>
        )
    }
}

class BrailleDecrypt extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            active: [
                false, false, false, false, false, false
            ],
            activeLetter: ''
        }
    }

    toggleActive(slot) {
        let active = this.state.active.slice();
        active[slot] = !active[slot];

        const asBinary = active.map((e) => e ? "1" : "0").join("");

        let activeLetter;

        if (asBinary === '000000') {
            activeLetter = '';
        } else if (asBinary in BrailleDecrypt.#binaryToBraille) {
            activeLetter = BrailleDecrypt.#binaryToBraille[asBinary][1] + ' ' + BrailleDecrypt.#binaryToBraille[asBinary][0]
        } else {
            activeLetter = '? ?'
        }

        this.setState({
            active: active,
            activeLetter: activeLetter,
        });
    }

    submitLetter() {
        const asBinary = this.state.active.map((e) => e ? "1" : "0").join("");

        this.setState({
            active: [false, false, false, false, false, false],
            activeLetter: '',
        });

        let inputBrailleBin = this.props.inputBrailleBin.slice()
        inputBrailleBin.push(asBinary);

        this.props.handleChange(inputBrailleBin);
    }

    static #binaryToBraille;
    static {
        this.#binaryToBraille = {};

        for (let braille of brailleCode) {
            this.#binaryToBraille[braille[3]] = braille;
        }
    }

    render() {
        return (
            <>
                <table className="borderless compact">
                    <tbody>
                    <tr>
                        <td>
                            <button className={this.state.active[0] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(0)}>1</button>
                        </td>
                        <td>
                            <button className={this.state.active[3] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(3)}>4</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button className={this.state.active[1] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(1)}>2</button>
                        </td>
                        <td>
                            <button className={this.state.active[4] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(4)}>5</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button className={this.state.active[2] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(2)}>3</button>
                        </td>
                        <td>
                            <button className={this.state.active[5] ? 'button-primary big-circle' : 'big-circle'} onClick={() => this.toggleActive(5)}>6</button>
                        </td>
                    </tr>
                    <tr>
                        <td className="monospace">
                            {this.state.activeLetter}
                        </td>
                        <td>
                            <button onClick={() => this.submitLetter()}>&#x27A4;</button>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <label>Vstup:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asGlyphs}/>
                </div>

                <div>
                    <label>Text:</label>
                    <input type="text" className="monospace u-full-width" readOnly={true} value={this.props.asPlaintext}/>
                </div>
            </>
        )
    }
}

class Braille extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'decrypt',

            inputPlaintext: '',
            inputBrailleBin: [],

            asGlyphs: '',
            asPlaintext: '',

            asNumbers: '',
            asBinary: '',
            asDecimal: '',
        };

    }

    handleChangeEncrypt(inputPlaintext) {
        inputPlaintext = inputPlaintext.toUpperCase();

        this.propagateEncryptDecrypt(
            Braille.#encodePlainToBinary(inputPlaintext),
            inputPlaintext
        );
    }

    handleChangeDecrypt(inputBrailleBin) {
        this.propagateEncryptDecrypt(
            inputBrailleBin,
            Braille.#encodeBinaryToPlain(inputBrailleBin).replaceAll('?', '')
        );
    }

    propagateEncryptDecrypt(inputBrailleBin, inputPlaintext) {
        this.setState({
            inputBrailleBin: inputBrailleBin,
            inputPlaintext: inputPlaintext,

            asGlyphs: Braille.#encodeBinaryToGlyphs(inputBrailleBin),
            asPlaintext: Braille.#encodeBinaryToPlain(inputBrailleBin),

            asNumbers: Braille.#encodeNumbers(inputPlaintext),
            asBinary: Braille.#encodeBinary(inputPlaintext),
            asDecimal: Braille.#encodeDecimal(inputPlaintext),
        })
    }

    componentDidMount() {
        onComponentDidMount(this, 'Braille');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        onComponentDidUpdate(this, 'Braille');
    }

    onTabChange(tab) {
        this.setState({
            view: tab,
        })
    }

    render() {
        let view;
        switch (this.state.view) {

            case 'encrypt':
                view = <BrailleEncrypt inputPlaintext={this.state.inputPlaintext}
                                       asGlyphs={this.state.asGlyphs}
                                       asNumbers={this.state.asNumbers}
                                       asBinary={this.state.asBinary}
                                       asDecimal={this.state.asDecimal}
                                       handleChange={(p) => this.handleChangeEncrypt(p)}/>
                break;

            case 'decrypt':
                view = <BrailleDecrypt inputBrailleBin={this.state.inputBrailleBin}
                                       asGlyphs={this.state.asGlyphs}
                                       asPlaintext={this.state.asPlaintext}
                                       handleChange={(m) => this.handleChangeDecrypt(m)}/>
                break;

            case 'reference':
                view = <BrailleReference/>
                break;
            default:
                throw new Error("Illegal view " + this.state.view);
        }

        return (
            <Cipher title={this.props.title}
                    selectedView={this.state.view}
                    onTabChange={(tab) => this.onTabChange(tab)}
                    onToggleMenu={this.props.onToggleMenu}>
                {view}
            </Cipher>
        )
    }

    static #plainToBraille;
    static #binaryToBraille;
    static {
        this.#plainToBraille = {};
        this.#binaryToBraille = {};

        for (let braille of brailleCode) {
            this.#plainToBraille[braille[0]] = braille;
            this.#binaryToBraille[braille[3]] = braille;
        }
    }

    static #encodeBinaryToGlyphs(binary) {
        return binary
            .map(b => binToGlyph.get(b))
            .join(' ');
    }

    static #encodeBinaryToPlain(binary) {
        return binary
            .map(b => b in Braille.#binaryToBraille ? Braille.#binaryToBraille[b][0] : '?')
            .join('');
    }

    static #encodePlainToBinary(plaintext) {
        return plaintext
            .split('')
            .filter(c => c in Braille.#plainToBraille)
            .map(
                c => Braille.#plainToBraille[c][3]
            );
    }

    static #encodeNumbers(plaintext) {
        return plaintext
            .toUpperCase()
            .split('')
            .filter(c => c in Braille.#plainToBraille)
            .map(
                c => Braille.#plainToBraille[c][2]
            )
            .join(', ');
    }

    static #encodeBinary(plaintext) {
        return plaintext
            .toUpperCase()
            .split('')
            .filter(c => c in Braille.#plainToBraille)
            .map(
                c => Braille.#plainToBraille[c][3]
            )
            .join(', ');
    }

    static #encodeDecimal(plaintext) {
        return plaintext
            .toUpperCase()
            .split('')
            .filter(c => c in Braille.#plainToBraille)
            .map(
                c => Braille.#plainToBraille[c][4]
            )
            .join(', ');
    }
}

export default Braille;
