import React from "react";

/*
see https://svgtojsx.com/ for conversion
 */

// @formatter:off
function Semaphore12(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11911">
                    <rect height="20" id="rect8008" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(4.360635e-25,1,-1,4.360635e-25,0,0)" x="240" y="-360"/>
                    <rect height="10" id="rect8010" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(3.052445e-25,1,-1,6.229479e-25,0,0)" x="300" y="-355"/>
                    <path id="path8014" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                    <path id="path8016" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 345,410 z "/>
                    <path id="path11836" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12037">
                    <rect height="20.000006" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999323" y="-363.55347"/>
                    <rect height="10.000003" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000088" y="-358.55347"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path11997" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore13(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4">
                <g id="armL">
                    <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                    <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                    <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                    <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
                </g>
            </defs>
            <circle cx="300" cy="210" r="25"/>
            <rect height="160" width="70" rx="20" ry="20" x="265" y="240"/>
            <rect height="20" width="70" rx="10" ry="10" x="190" y="240"/>
            <g transform="rotate(90 350,250)">
                <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
            </g>
            <path d="M0,244.5h200.5v11h-110v90h-91z"/>
            <path d="M.5,255.5h89v89h-89z" fill="#ff0"/>
            <path d="M.5,255.5h89l-89,89z" fill="#f00"/>
        </svg>
    );
}

function Semaphore14(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11911">
                    <rect height="20" id="rect8008" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(4.360635e-25,1,-1,4.360635e-25,0,0)" x="240" y="-360"/>
                    <rect height="10" id="rect8010" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(3.052445e-25,1,-1,6.229479e-25,0,0)" x="300" y="-355"/>
                    <path id="path8014" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                    <path id="path8016" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 345,410 z "/>
                    <path id="path11836" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore15(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11911">
                    <rect height="20" id="rect8008" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(4.360635e-25,1,-1,4.360635e-25,0,0)" x="240" y="-360"/>
                    <rect height="10" id="rect8010" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(3.052445e-25,1,-1,6.229479e-25,0,0)" x="300" y="-355"/>
                    <path id="path8014" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                    <path id="path8016" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 345,410 z "/>
                    <path id="path11836" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 255,410 L 345,410 L 345,500 L 255,500 L 255,410 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12016">
                    <rect height="20" id="rect7915" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.190589e-17,-1,1,-1.190589e-17,0,0)" x="-260" y="240"/>
                    <rect height="10" id="rect7917" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-1.223319e-17,-1,1,-1.143833e-17,0,0)" x="-200" y="245"/>
                    <path id="path7921" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                    <path id="path7923" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,-5.3290705e-015 z "/>
                    <path id="path12003" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore16(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11864">
                    <rect height="20.000006" id="rect7980" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="60.710747" y="414.26401"/>
                    <rect height="10.000003" id="rect7982" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="120.71076" y="419.26401"/>
                    <path id="path7986" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                    <path id="path7988" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 466.67265,140.39831 z "/>
                    <path id="path11842" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12044">
                    <rect height="20" id="rect7901" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.090978e-18,1,-1,-1.090978e-18,0,0)" x="240" y="-260"/>
                    <rect height="10" id="rect7903" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-7.636846e-19,1,-1,-1.55854e-18,0,0)" x="300" y="-255"/>
                    <path id="path7907" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                    <path id="path7909" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,500 L 255,500 L 255,410 L 345,500 z "/>
                    <path id="path12001" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore17(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12044">
                    <rect height="20" id="rect7901" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.090978e-18,1,-1,-1.090978e-18,0,0)" x="240" y="-260"/>
                    <rect height="10" id="rect7903" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-7.636846e-19,1,-1,-1.55854e-18,0,0)" x="300" y="-255"/>
                    <path id="path7907" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                    <path id="path7909" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,500 L 255,500 L 255,410 L 345,500 z "/>
                    <path id="path12001" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore18(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g12189">
                    <rect height="20.000006" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26416" y="-80.71077"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path11844" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12044">
                    <rect height="20" id="rect7901" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.090978e-18,1,-1,-1.090978e-18,0,0)" x="240" y="-260"/>
                    <rect height="10" id="rect7903" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-7.636846e-19,1,-1,-1.55854e-18,0,0)" x="300" y="-255"/>
                    <path id="path7907" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                    <path id="path7909" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 345,500 L 255,500 L 255,410 L 345,500 z "/>
                    <path id="path12001" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 345,410 L 345,500 L 255,500 L 255,410 L 345,410 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore23(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11918">
                    <rect height="20.000006" id="rect11714" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-58.083195" y="-411.63672"/>
                    <rect height="10.000003" id="rect11716" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="1.9168251" y="-406.63672"/>
                    <path id="path11720" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 272.03804,430.31241 L 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 272.03804,430.31241 z "/>
                    <path id="path11722" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 208.39841,493.95205 z "/>
                    <path id="path11846" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 272.03804,430.31241 L 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 272.03804,430.31241 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12030">
                    <rect height="20" id="rect7742" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="scale(-1,-1)" x="-260" y="-260"/>
                    <rect height="10" id="rect7744" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="scale(-1,-1)" x="-200" y="-255"/>
                    <path id="path7762" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 90,345 L -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L 90,345 z "/>
                    <path id="path7777" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L -2.220446e-016,345 z "/>
                    <path id="path11995" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 90,345 L -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L 90,345 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore24(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11918">
                    <rect height="20.000006" id="rect11714" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-58.083195" y="-411.63672"/>
                    <rect height="10.000003" id="rect11716" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="1.9168251" y="-406.63672"/>
                    <path id="path11720" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 272.03804,430.31241 L 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 272.03804,430.31241 z "/>
                    <path id="path11722" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 208.39841,493.95205 z "/>
                    <path id="path11846" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 272.03804,430.31241 L 208.39841,493.95205 L 144.75878,430.31241 L 208.39841,366.67278 L 272.03804,430.31241 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore25(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11857">
                    <rect height="20" id="rect7959" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(1.111307e-18,-1,1,1.111307e-18,0,0)" x="-260" y="340"/>
                    <rect height="10" id="rect7961" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(1.111307e-18,-1,1,1.111306e-18,0,0)" x="-200" y="345"/>
                    <path id="path7965" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 445,90 L 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 445,90 z "/>
                    <path id="path7967" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 355,90 z "/>
                    <path id="path11840" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 445,90 L 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 445,90 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12037">
                    <rect height="20.000006" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999323" y="-363.55347"/>
                    <rect height="10.000003" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000088" y="-358.55347"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path11997" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore26(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11864">
                    <rect height="20.000006" id="rect7980" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="60.710747" y="414.26401"/>
                    <rect height="10.000003" id="rect7982" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="120.71076" y="419.26401"/>
                    <path id="path7986" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                    <path id="path7988" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 466.67265,140.39831 z "/>
                    <path id="path11842" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12037">
                    <rect height="20.000006" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999323" y="-363.55347"/>
                    <rect height="10.000003" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000088" y="-358.55347"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path11997" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore27(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12037">
                    <rect height="20.000006" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999323" y="-363.55347"/>
                    <rect height="10.000003" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000088" y="-358.55347"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path11997" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore28(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g12189">
                    <rect height="20.000006" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26416" y="-80.71077"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path11844" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12037">
                    <rect height="20.000006" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999323" y="-363.55347"/>
                    <rect height="10.000003" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000088" y="-358.55347"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path11997" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore34(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11850">
                    <rect height="20.000006" id="rect11728" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-411.63672" y="38.083328"/>
                    <rect height="10.000003" id="rect11730" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-351.63672" y="43.083332"/>
                    <path id="path11734" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 137.68782,204.03804 L 74.048185,140.39841 L 137.68782,76.758785 L 201.32745,140.39841 L 137.68782,204.03804 z "/>
                    <path id="path11736" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 74.048185,140.39841 L 137.68782,76.758785 L 201.32745,140.39841 L 74.048185,140.39841 z "/>
                    <path id="path11848" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 137.68782,204.03804 L 74.048185,140.39841 L 137.68782,76.758785 L 201.32745,140.39841 L 137.68782,204.03804 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12030">
                    <rect height="20" id="rect7742" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="scale(-1,-1)" x="-260" y="-260"/>
                    <rect height="10" id="rect7744" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="scale(-1,-1)" x="-200" y="-255"/>
                    <path id="path7762" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 90,345 L -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L 90,345 z "/>
                    <path id="path7777" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L -2.220446e-016,345 z "/>
                    <path id="path11995" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 90,345 L -2.220446e-016,345 L -2.7755576e-016,255 L 90,255 L 90,345 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore35(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4">
                <g id="armL">
                    <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                    <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                    <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                    <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
                </g>
            </defs>
            <circle cx="300" cy="210" r="25"/>
            <rect height="160" width="70" rx="20" ry="20" x="265" y="240"/>
            <rect height="20" width="70" rx="10" ry="10" x="190" y="240"/>
            <g transform="rotate(-90 350,250)">
                <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
            </g>
            <path d="M0,244.5h200.5v11h-110v90h-91z"/>
            <path d="M.5,255.5h89v89h-89z" fill="#ff0"/>
            <path d="M.5,255.5h89l-89,89z" fill="#f00"/>
        </svg>
    );
}

function Semaphore36(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4">
                <g id="armL">
                    <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                    <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                    <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                    <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
                </g>
            </defs>
            <circle cx="300" cy="210" r="25"/>
            <rect height="160" width="70" rx="20" ry="20" x="265" y="240"/>
            <rect height="20" width="70" rx="10" ry="10" x="190" y="240"/>
            <g transform="rotate(315 350,250)">
                <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
            </g>
            <path d="M0,244.5h200.5v11h-110v90h-91z"/>
            <path d="M.5,255.5h89v89h-89z" fill="#ff0"/>
            <path d="M.5,255.5h89l-89,89z" fill="#f00"/>
        </svg>
    );
}

function Semaphore37(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <circle cx="300" cy="210" r="25"/>
            <rect height="160" width="70" rx="20" ry="20" x="265" y="240"/>
            <g id="armL">
                <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
            </g>
            <g id="armR">
                <rect height="20" width="70" rx="10" ry="10" x="190" y="240"/>
                <path d="M0,244.5h200.5v11h-110v90h-91z"/>
                <path d="M.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M.5,255.5h89l-89,89z" fill="#f00"/>
            </g>
        </svg>
    );
}

function Semaphore38(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4">
                <g id="armL">
                    <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                    <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                    <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                    <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
                </g>
            </defs>
            <circle cx="300" cy="210" r="25"/>
            <rect height="160" width="70" rx="20" ry="20" x="265" y="240"/>
            <rect height="20" width="70" rx="10" ry="10" x="190" y="240"/>
            <g transform="rotate(45 350,250)">
                <rect height="20" width="70" rx="10" ry="10" x="340" y="240"/>
                <path d="M399,244.5h200.5v101h-91v-90h-110z"/>
                <path d="M509.5,255.5h89v89h-89z" fill="#ff0"/>
                <path d="M509.5,255.5l89,89v-89z" fill="#f00"/>
            </g>
            <path d="M0,244.5h200.5v11h-110v90h-91z"/>
            <path d="M.5,255.5h89v89h-89z" fill="#ff0"/>
            <path d="M.5,255.5h89l-89,89z" fill="#f00"/>
        </svg>
    );
}

function Semaphore45(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11857">
                    <rect height="20" id="rect7959" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(1.111307e-18,-1,1,1.111307e-18,0,0)" x="-260" y="340"/>
                    <rect height="10" id="rect7961" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(1.111307e-18,-1,1,1.111306e-18,0,0)" x="-200" y="345"/>
                    <path id="path7965" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 445,90 L 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 445,90 z "/>
                    <path id="path7967" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 355,90 z "/>
                    <path id="path11840" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 445,90 L 355,90 L 355,-3.8857806e-016 L 445,-2.7755576e-016 L 445,90 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore46(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11864">
                    <rect height="20.000006" id="rect7980" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="60.710747" y="414.26401"/>
                    <rect height="10.000003" id="rect7982" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="120.71076" y="419.26401"/>
                    <path id="path7986" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                    <path id="path7988" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 466.67265,140.39831 z "/>
                    <path id="path11842" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore47(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore56(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11864">
                    <rect height="20.000006" id="rect7980" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="60.710747" y="414.26401"/>
                    <rect height="10.000003" id="rect7982" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="120.71076" y="419.26401"/>
                    <path id="path7986" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                    <path id="path7988" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 466.67265,140.39831 z "/>
                    <path id="path11842" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12016">
                    <rect height="20" id="rect7915" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.190589e-17,-1,1,-1.190589e-17,0,0)" x="-260" y="240"/>
                    <rect height="10" id="rect7917" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-1.223319e-17,-1,1,-1.143833e-17,0,0)" x="-200" y="245"/>
                    <path id="path7921" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                    <path id="path7923" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,-5.3290705e-015 z "/>
                    <path id="path12003" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore57(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12016">
                    <rect height="20" id="rect7915" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.190589e-17,-1,1,-1.190589e-17,0,0)" x="-260" y="240"/>
                    <rect height="10" id="rect7917" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-1.223319e-17,-1,1,-1.143833e-17,0,0)" x="-200" y="245"/>
                    <path id="path7921" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                    <path id="path7923" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,-5.3290705e-015 z "/>
                    <path id="path12003" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore58(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g12189">
                    <rect height="20.000006" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26416" y="-80.71077"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path11844" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12016">
                    <rect height="20" id="rect7915" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" transform="matrix(-1.190589e-17,-1,1,-1.190589e-17,0,0)" x="-260" y="240"/>
                    <rect height="10" id="rect7917" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" transform="matrix(-1.223319e-17,-1,1,-1.143833e-17,0,0)" x="-200" y="245"/>
                    <path id="path7921" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                    <path id="path7923" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,-5.3290705e-015 z "/>
                    <path id="path12003" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 155,90 L 155,-5.3290705e-015 L 245,-6.4948047e-015 L 245,90 L 155,90 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore67(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12009">
                    <rect height="20.000006" id="rect11644" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="12.627343" y="366.18088"/>
                    <rect height="10.000003" id="rect11646" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="72.627365" y="371.18088"/>
                    <path id="path11650" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 462.31228,204.03815 L 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 462.31228,204.03815 z "/>
                    <path id="path11652" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 398.67265,140.39852 z "/>
                    <path id="path12005" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 462.31228,204.03815 L 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 462.31228,204.03815 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore68(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g12189">
                    <rect height="20.000006" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26416" y="-80.71077"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path11844" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12009">
                    <rect height="20.000006" id="rect11644" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="12.627343" y="366.18088"/>
                    <rect height="10.000003" id="rect11646" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="72.627365" y="371.18088"/>
                    <path id="path11650" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 462.31228,204.03815 L 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 462.31228,204.03815 z "/>
                    <path id="path11652" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 398.67265,140.39852 z "/>
                    <path id="path12005" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 462.31228,204.03815 L 398.67265,140.39852 L 462.31228,76.758885 L 525.95191,140.39852 L 462.31228,204.03815 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore78(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g11871">
                    <rect height="20" id="rect7724" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="10" ry="10" x="340" y="240"/>
                    <rect height="10" id="rect7726" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200" x="400" y="245"/>
                    <path id="path7785" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                    <path id="path7787" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 510,255 L 600,255 L 600,345 L 510,255 z "/>
                    <path id="path11838" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 510,345 L 510,255 L 600,255 L 600,345 L 510,345 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12051">
                    <rect height="20.000006" id="rect11686" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="366.18088" y="-32.627491"/>
                    <rect height="10.000003" id="rect11688" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="426.18091" y="-27.627489"/>
                    <path id="path11692" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 327.96206,430.31228 L 391.60169,366.67265 L 455.24132,430.31228 L 391.60169,493.95191 L 327.96206,430.31228 z "/>
                    <path id="path11694" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 391.60169,366.67265 L 455.24132,430.31228 L 391.60169,493.95191 L 391.60169,366.67265 z "/>
                    <path id="path12007" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 327.96206,430.31228 L 391.60169,366.67265 L 455.24132,430.31228 L 391.60169,493.95191 L 327.96206,430.31228 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore48(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
            </g>
            <g id="layer4" style={{"display":"inline"}}>
                <g id="g12189">
                    <rect height="20.000006" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26416" y="-80.71077"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path11844" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
            <g id="layer5" style={{"display":"inline"}}>
                <g id="g12023">
                    <rect height="20.000006" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000023" rx="10.000003" ry="10.000003" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55331" y="-10.000073"/>
                    <rect height="10.000003" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"0.9999997","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55331" y="-5.0000725"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path11999" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeLinejoin":"round","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
            </g>
        </svg>
    );
}

function Semaphore99(props) {
    return (
        <svg height="50" id="svg2" width="60" viewBox="0 0 500 600" version="1.0" xmlns="http://www.w3.org/2000/svg" {...props}>
            <defs id="defs4"/>
            <g id="layer1">
                <path id="path7712" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} d="M 275 200 A 25 25 0 1 1  225,200 A 25 25 0 1 1  275 200 z" transform="translate(50,10)"/>
                <path id="path11296" style={{"opacity":"1","fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"5","markerStart":"url(#Arrow1Mstart)","markerEnd":"url(#Arrow1Mend)","strokeMiterlimit":"4","strokeDasharray":"10, 5","strokeDashoffset":"0","strokeOpacity":"1"}} d="M 72.275734,369.45425 A 278,278 0 0 1 22,209.99999"/>
                <path id="path11298" style={{"opacity":"1","fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"5","markerStart":"url(#Arrow1Mstart)","markerEnd":"url(#Arrow1Mend)","strokeMiterlimit":"4","strokeDasharray":"10, 5","strokeDashoffset":"0","strokeOpacity":"1"}} d="M 578,210 A 278,278 0 0 1 527.72427,369.45425"/>
                <rect height="160" id="rect7718" style={{"opacity":"1","fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70" rx="20" ry="21.333334" x="265" y="240"/>
                <g id="g8688">
                    <rect height="20.000011" id="rect7827" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000046" rx="10.000006" ry="10.000006" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="-9.9999352" y="-363.55359"/>
                    <rect height="10.000006" id="rect7829" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(-0.707107,0.707107,-0.707107,-0.707107,0,0)" x="50.000103" y="-358.55359"/>
                    <path id="path7833" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                    <path id="path7835" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 140.39841,493.95205 z "/>
                    <path id="path8319" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1","strokeLinejoin":"round"}} d="M 204.03804,430.31241 L 140.39841,493.95205 L 76.758785,430.31241 L 140.39841,366.67278 L 204.03804,430.31241 z "/>
                </g>
                <g id="g8667">
                    <rect height="20.000011" id="rect7841" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000046" rx="10.000006" ry="10.000006" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-363.55344" y="-10.000076"/>
                    <rect height="10.000006" id="rect7843" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(-0.707107,-0.707107,0.707107,-0.707107,0,0)" x="-303.55341" y="-5.0000739"/>
                    <path id="path7847" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                    <path id="path7849" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 6.047985,140.39841 z "/>
                    <path id="path8321" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1","strokeLinejoin":"round"}} d="M 69.687615,204.03804 L 6.047985,140.39841 L 69.687615,76.758785 L 133.32725,140.39841 L 69.687615,204.03804 z "/>
                </g>
                <g id="g8674">
                    <rect height="20.000011" id="rect7980" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000046" rx="10.000006" ry="10.000006" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="60.710766" y="414.26413"/>
                    <rect height="10.000006" id="rect7982" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,-0.707107,0.707107,0.707107,0,0)" x="120.7108" y="419.26413"/>
                    <path id="path7986" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                    <path id="path7988" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 466.67265,140.39831 z "/>
                    <path id="path8323" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1","strokeLinejoin":"round"}} d="M 530.31228,204.03794 L 466.67265,140.39831 L 530.31228,76.758685 L 593.95191,140.39831 L 530.31228,204.03794 z "/>
                </g>
                <g id="g8681">
                    <rect height="20.000011" id="rect7994" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"2.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="70.000046" rx="10.000006" ry="10.000006" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="414.26428" y="-80.710793"/>
                    <rect height="10.000006" id="rect7996" style={{"fill":"#000000","fillOpacity":"1","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="200.00012" transform="matrix(0.707107,0.707107,-0.707107,0.707107,0,0)" x="474.26434" y="-75.710793"/>
                    <path id="path8000" style={{"fill":"#ffff00","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                    <path id="path8002" style={{"fill":"#ff0000","fillOpacity":"1","fillRule":"evenodd","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeOpacity":"1"}} d="M 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 459.60172,366.67265 z "/>
                    <path id="path8325" style={{"fill":"#ffff00","fillOpacity":"0","fillRule":"evenodd","stroke":"#000000","strokeWidth":"1","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1","strokeLinejoin":"round"}} d="M 395.96209,430.31228 L 459.60172,366.67265 L 523.24135,430.31228 L 459.60172,493.95191 L 395.96209,430.31228 z "/>
                </g>
            </g>
        </svg>
    );
}

function IcsA(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <path d="M1.25,1.25h597.5l-148.75,298.75l148.75,298.75h-597.5z" fill="#fff"/>
            <path d="M300,1.25h297.5l-148.75,298.75l148.75,298.75h-297.5z" fill="#00f"/>
            <path d="M1.25,1.25h597.5l-148.75,298.75l148.75,298.75h-597.5z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function IcsB(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <path d="M448,300 596,599H1V1H596z" fill="#F00" stroke="#000" strokeWidth="2"/>
        </svg>
    );
}

function IcsC(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 5 5" {...props}>
            <path d="M0,0h5v5h-5z" fill="#00f"/>
            <path d="M0,1h5v3h-5z" fill="#fff"/>
            <path d="M0,2h5v1h-5z" fill="#f00"/>
        </svg>
    );
}

function IcsD(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="yellow"/>
            <rect height="360" width="600" fill="#0000ff" y="120"/>
        </svg>
    );
}

function IcsE(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="300" width="600" fill="blue" x="0" y="0"/>
            <rect height="300" id="a" width="600" fill="red" x="0" y="300"/>
        </svg>
    );
}

function IcsF(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 4 4" {...props}>
            <path d="M0,0h4v4h-4z" fill="#fff" stroke="#000" strokeWidth=".017"/>
            <path d="M2,0l2,2l-2,2l-2,-2" fill="#f00"/>
        </svg>
    );
}

function IcsG(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 6 6" {...props}>
            <path d="M0,0h6v6h-6z" fill="#ff0"/>
            <path d="M1,0v6h1v-6h1v6h1v-6h1v6h1v-6z" fill="#00f"/>
        </svg>
    );
}

function IcsH(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <path d="m1,0h599v600H1" fill="#F00"/>
            <path d="m0,0h300v600H0" fill="#FFF"/>
        </svg>
    );
}

function IcsI(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <path d="m0,0h600v600H0" fill="#FF0" stroke="#000" strokeWidth="2"/>
            <circle cx="300" cy="300" r="150"/>
        </svg>
    );
}

function IcsJ(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="blue" x="0" y="0"/>
            <rect height="200" width="600" fill="white" x="0" y="200"/>
        </svg>
    );
}

function IcsK(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 2 2" {...props}>
            <path d="M0,0h1v2H0z" fill="#ff0"/>
            <path d="M1,0h1v2H1z" fill="#00f"/>
        </svg>
    );
}

function IcsL(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 2 2" {...props}>
            <path d="M0,0h2v2h-2z" fill="#ff0"/>
            <path d="M0,1h2v-1h-1v2h-1z"/>
        </svg>
    );
}

function IcsM(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="#00f"/>
            <g stroke="#fff" strokeWidth="105">
                <path d="M 0,0 L 600,600 z"/>
                <path d="M 0,600 L 600,0 z"/>
                <rect height="600" width="600" fill="none" stroke="#000" strokeWidth="2"/>
            </g>
        </svg>
    );
}

function IcsN(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 4 4" {...props}>
            <path d="M0,0h4v4H0z" fill="#fff"/>
            <path d="M0,3v-3h1v4h1v-4h1v4h1v-3h-4v1h4v1z" fill="#00f"/>
        </svg>
    );
}

function IcsO(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 1 1" {...props}>
            <path d="M0,0h1v1h-1z" fill="red"/>
            <path d="M0,0v1h1z" fill="#ff0"/>
        </svg>
    );
}

function IcsP(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 3 3" {...props}>
            <path d="M0,0h3v3h-3" fill="#00f"/>
            <path d="M1,1h1v1h-1" fill="#fff"/>
        </svg>
    );
}

function IcsQ(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="yellow" x="0" y="0"/>
        </svg>
    );
}

function IcsR(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 5 5" {...props}>
            <path d="M0,0h5v5h-5z" fill="#f00"/>
            <path d="M2,0h1v2h2v1h-2v2h-1v-2h-2v-1h2z" fill="#ff0"/>
        </svg>
    );
}

function IcsS(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 3 3" {...props}>
            <path d="M0,0h3v3h-3z" fill="#fff" stroke="#000" strokeWidth=".0125"/>
            <path d="M1,1h1v1h-1z" fill="#00f"/>
        </svg>
    );
}

function IcsT(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="200" fill="red" x="0" y="0"/>
            <rect height="600" width="200" fill="white" x="200" y="0"/>
            <rect height="600" width="200" fill="blue" x="400" y="0"/>
        </svg>
    );
}

function IcsU(props) {
    return (
        <svg height="30" width="30"  viewBox="0 0 2 2" {...props}>
            <path d="M0,0h2v2h-2z" fill="#f00"/>
            <path d="M0,1h2v-1h-1v2h-1z" fill="#fff"/>
        </svg>
    );
}

function IcsV(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="#fff"/>
            <g stroke="#f00" strokeWidth="105">
                <path d="M 0,0 L 600,600 z"/>
                <path d="M 0,600 L 600,0 z"/>
                <rect height="600" width="600" fill="none" stroke="#000" strokeWidth="2"/>
            </g>
        </svg>
    );
}

function IcsW(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="#00f" stroke="#000" strokeWidth="2.5"/>
            <rect height="360" width="360" fill="#fff" x="120" y="120"/>
            <rect height="120" width="120" fill="#f00" x="240" y="240"/>
        </svg>
    );
}

function IcsX(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 5 5" {...props}>
            <path d="M0,0h5v5h-5z" fill="#fff"/>
            <path d="M2,0h1v2h2v1h-2v2h-1v-2h-2v-1h2z" fill="#00f"/>
            <path d="M0,0h5v5h-5z" fill="none" stroke="#000" strokeWidth=".02"/>
        </svg>
    );
}

function IcsY(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 600 600" {...props}>
            <rect height="600" width="600" fill="#F00"/>
            <path d="M0,0 600,600" stroke="#FF0" strokeDasharray="84,86" strokeWidth="850"/>
            <path d="M0,0H600V600H0z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function IcsZ(props) {
    return (
        <svg height="30" width="30" viewBox="0 0 2 2" {...props}>
            <path d="M0,0h2v2h-2z" fill="#ff0"/>
            <path d="M0,0l1,1l-1,1z"/>
            <path d="M0,2l1,-1l1,1z" fill="#f00"/>
            <path d="M2,2l-1,-1l1,-1z" fill="#00f"/>
        </svg>
    );
}

function Ics0(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 432 240" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#ff0"/>
            <path d="M177.5,26l184.5,26v196l-184.5,26z" fill="#f00"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics1(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#fff" stroke="#000" strokeWidth="2.5"/>
            <circle cx="150" cy="150" fill="#f00" r="75.3"/>
        </svg>
    );
}

function Ics2(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#00f" stroke="#000" strokeWidth="2.5"/>
            <circle cx="150" cy="150" fill="#fff" r="75.3"/>
        </svg>
    );
}

function Ics3(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#fff"/>
            <path d="M1.25,1.25l176.25,25v247.5l-176.25,25z" fill="#f00"/>
            <path d="M362,52l176.25,25v147.5l-176.25,25z" fill="#00f"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics4(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#f00"/>
            <path d="M1.25,112.5h111.5v-96l75,10.4v85.6h351v75h-351v85.6l-75,10.4v-96h-111.5" fill="#fff"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics5(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#ff0"/>
            <path d="M270,39v222l269,-37.75v-146.5z" fill="#00f"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics6(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z"/>
            <path d="M1.25,150h537.5v73.75l-537.5,75z" fill="#fff"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics7(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#ff0"/>
            <path d="M1.25,150h537.5v73.75l-537.5,75z" fill="#f00"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics8(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#fff"/>
            <path d="M1.25,112.5h111.5v-96l75,10.4v85.6h351v75h-351v85.6l-75,10.4v-96h-111.5" fill="#f00"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

function Ics9(props) {
    return (
        <svg height="30" width="54" viewBox="0 0 540 300" {...props}>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="#f00"/>
            <path d="M1.25,150v-148.75l269,37.75v111z" fill="#fff"/>
            <path d="M270,150v111l269,-37.75v-73.25z" fill="#ff0"/>
            <path d="M270,150v-111l269,37.75v73.25z"/>
            <path d="M1.25,1.25l537.5,75v147.5l-537.5,75z" fill="none" stroke="#000" strokeWidth="2.5"/>
        </svg>
    );
}

// @formatter:on

export function semaphoreByCode(code, props) {
    switch (code) {
        case '12':
            return Semaphore12(props);
        case '13':
            return Semaphore13(props);
        case '14':
            return Semaphore14(props);
        case '15':
            return Semaphore15(props);
        case '16':
            return Semaphore16(props);
        case '17':
            return Semaphore17(props);
        case '18':
            return Semaphore18(props);
        case '23':
            return Semaphore23(props);
        case '24':
            return Semaphore24(props);
        case '57':
            return Semaphore57(props);
        case '25':
            return Semaphore25(props);
        case '26':
            return Semaphore26(props);
        case '27':
            return Semaphore27(props);
        case '28':
            return Semaphore28(props);
        case '34':
            return Semaphore34(props);
        case '35':
            return Semaphore35(props);
        case '36':
            return Semaphore36(props);
        case '37':
            return Semaphore37(props);
        case '38':
            return Semaphore38(props);
        case '45':
            return Semaphore45(props);
        case '46':
            return Semaphore46(props);
        case '56':
            return Semaphore56(props);
        case '58':
            return Semaphore58(props);
        case '67':
            return Semaphore67(props);
        case '68':
            return Semaphore68(props);
        case '47':
            return Semaphore47(props);
        case '78':
            return Semaphore78(props);
        case '48':
            return Semaphore48(props);
        case '99':
            return Semaphore99(props);

        default:
            return '?';
    }
}

export function icsByChar(c, props) {
    switch (c) {
        case 'A':
            return IcsA(props);
        case 'B':
            return IcsB(props);
        case 'C':
            return IcsC(props);
        case 'D':
            return IcsD(props);
        case 'E':
            return IcsE(props);
        case 'F':
            return IcsF(props);
        case 'G':
            return IcsG(props);
        case 'H':
            return IcsH(props);
        case 'I':
            return IcsI(props);
        case 'J':
            return IcsJ(props);
        case 'K':
            return IcsK(props);
        case 'L':
            return IcsL(props);
        case 'M':
            return IcsM(props);
        case 'N':
            return IcsN(props);
        case 'O':
            return IcsO(props);
        case 'P':
            return IcsP(props);
        case 'Q':
            return IcsQ(props);
        case 'R':
            return IcsR(props);
        case 'S':
            return IcsS(props);
        case 'T':
            return IcsT(props);
        case 'U':
            return IcsU(props);
        case 'V':
            return IcsV(props);
        case 'W':
            return IcsW(props);
        case 'X':
            return IcsX(props);
        case 'Y':
            return IcsY(props);
        case 'Z':
            return IcsZ(props);
        case '0':
            return Ics0(props);
        case '1':
            return Ics1(props);
        case '2':
            return Ics2(props);
        case '3':
            return Ics3(props);
        case '4':
            return Ics4(props);
        case '5':
            return Ics5(props);
        case '6':
            return Ics6(props);
        case '7':
            return Ics7(props);
        case '8':
            return Ics8(props);
        case '9':
            return Ics9(props);

        default:
            return '?';
    }
}
