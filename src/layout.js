import React from "react";


export function Cipher(props) {
    return (
        <>
            <h2>{props.title}
                <button className="u-pull-right" onClick={props.onToggleMenu}>&equiv;</button>
            </h2>

            <hr/>

            <div className="row">
                <button onClick={() => props.onTabChange('encrypt')} className={props.selectedView === 'encrypt' ? 'active mr-1' : 'mr-1'}>Enc</button>
                <button onClick={() => props.onTabChange('decrypt')} className={props.selectedView === 'decrypt' ? 'active mr-1' : 'mr-1'}>Dec</button>
                {props.hideReference ? '' :
                    <button onClick={() => props.onTabChange('reference')} className={props.selectedView === 'reference' ? 'active mr-1' : 'mr-1'}>Ref</button>
                }
            </div>

            <hr/>

            <div>
                {props.children}
            </div>
        </>
    )
}

Cipher.defaultProps = {
    hideReference: false,
}
