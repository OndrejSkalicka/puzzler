export const alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

const diacritics = new Map([
    ['á', 'a'],
    ['č', 'c'],
    ['ď', 'd'],
    ['é', 'e'],
    ['ě', 'e'],
    ['í', 'i'],
    ['ň', 'n'],
    ['ó', 'o'],
    ['ř', 'r'],
    ['š', 's'],
    ['ť', 't'],
    ['ú', 'u'],
    ['ů', 'u'],
    ['ý', 'y'],
    ['ž', 'z'],
    ['Á', 'A'],
    ['Č', 'C'],
    ['Ď', 'D'],
    ['É', 'E'],
    ['Ě', 'E'],
    ['Í', 'I'],
    ['Ň', 'N'],
    ['Ó', 'O'],
    ['Ř', 'R'],
    ['Š', 'S'],
    ['Ť', 'T'],
    ['Ú', 'U'],
    ['Ů', 'U'],
    ['Ý', 'Y'],
    ['Ž', 'Z'],
])

export function forceUpperAlpha(input) {
    return input.split('')
        .map(c => diacritics.has(c) ? diacritics.get(c) : c)
        .map(c => c.toUpperCase())
        .filter(c => alpha.includes(c))
        .join('');
}
